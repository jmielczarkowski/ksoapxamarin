﻿using System;
using System.IO;
using System.Text;

namespace kSOAPXamarin.Entities
{
    /// <summary>
    /// Base64 class.
    /// </summary>
    public static class Base64
    {
        private static readonly char[] CharTab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".ToCharArray();

        public static string Encode(byte[] data)
        {
            return Encode(data, 0, data.Length, null).ToString();
        }

        /// <summary>
        /// Encodes the part of the given byte array denoted by start and
        /// len to the Base64 format.  The encoded data is Appended to the
        /// given StringBuffer. If no StringBuffer is given, a new one is
        /// created automatically. The StringBuffer is the return value of
        /// this method.
        /// </summary>
        public static StringBuilder Encode(byte[] data, int start, int len, StringBuilder buf)
        {
            if (buf == null)
                buf = new StringBuilder(data.Length*3/2);

            var end = len - 3;
            var i = start;
            var n = 0;

            while (i <= end)
            {
                var d = ((data[i] & 0x0ff) << 16)
                    | ((data[i + 1] & 0x0ff) << 8)
                    | (data[i + 2] & 0x0ff);

                buf.Append(CharTab[(d >> 18) & 63]);
                buf.Append(CharTab[(d >> 12) & 63]);
                buf.Append(CharTab[(d >> 6) & 63]);
                buf.Append(CharTab[d & 63]);

                i += 3;

                if (n++ >= 14)
                {
                    n = 0;
                    buf.Append("\r\n");
                }
            }

            if (i == start + len - 2)
            {
                var d = (( data[i] & 0x0ff) << 16)
                    | ((data[i + 1] & 255) << 8);

                buf.Append(CharTab[(d >> 18) & 63]);
                buf.Append(CharTab[(d >> 12) & 63]);
                buf.Append(CharTab[(d >> 6) & 63]);
                buf.Append("=");
            }
            else if (i == start + len - 1)
            {
                var d = (data[i] & 0x0ff) << 16;

                buf.Append(CharTab[(d >> 18) & 63]);
                buf.Append(CharTab[(d >> 12) & 63]);
                buf.Append("==");
            }

            return buf;
        }

        private static int Decode(char c)
        {
            if (c >= 'A' && c <= 'Z')
                return c - 65;
            if (c >= 'a' && c <= 'z')
                return c - 97 + 26;
            if (c >= '0' && c <= '9')
                return c - 48 + 26 + 26;
            
            switch (c)
            {
                case '+':
                    return 62;
                case '/':
                    return 63;
                case '=':
                    return 0;
                default:
                    throw new Exception($"unexpected code: {c}");
            }
        }

        /// <summary>
        /// Decodes the given Base64 encoded String to a new byte array. 
        /// The byte array holding the decoded data is returned. 
        /// </summary>
        public static byte[] Decode(string s)
        {

            var memoryStream = new MemoryStream();
            try
            {
                Decode(s, memoryStream);
            }
            catch (IOException)
            {
                throw new Exception();
            }
            return memoryStream.ToArray();
        }

        public static void Decode(string s, Stream os)
        {
            var i = 0;
            var len = s.Length;

            while (true)
            {
                while (i < len && s[i] <= ' ')
                    i++;

                if (i == len)
                    break;

                var tri =
                    (Decode(s[i]) << 18)
                    + (Decode(s[i + 1]) << 12)
                    + (Decode(s[i + 2]) << 6)
                    + (Decode(s[i + 3]));

                os.WriteByte((byte) ((tri >> 16) & 255));
                if (s[i + 2] == '=')
                    break;
                os.WriteByte((byte) ((tri >> 8) & 255));
                if (s[i + 3] == '=')
                    break;
                os.WriteByte((byte) (tri & 255));

                i += 4;
            }
        }
    }
}
