﻿using System;
using System.Text;

namespace kSOAPXamarin.Entities
{
    /// <summary>
    /// IsoDate class.
    /// </summary>
    public class IsoDate
    {
        public static readonly int Date = 1;
        public static readonly int Time = 2;
        public static readonly int DateTime = 3;

        private static void Append(StringBuilder buf, int i)
        {
            buf.Append((char) ('0' + i/10));
            buf.Append((char) ('0' + i%10));
        }

        public static string DateToString(DateTime date, int type)
        {
            date = date.ToUniversalTime();
            var buf = new StringBuilder();

            if ((type & Date) != 0)
            {
                var year = date.Year;
                Append(buf, year/100);
                Append(buf, year%100);
                buf.Append('-');
                Append(buf, date.Month);
                buf.Append('-');
                Append(buf, date.Day);

                if (type == DateTime)
                    buf.Append("T");
            }

            if ((type & Time) != 0)
            {
                Append(buf, date.Hour);
                buf.Append(':');
                Append(buf, date.Minute);
                buf.Append(':');
                Append(buf, date.Second);
                buf.Append('.');
                var ms = date.Millisecond;
                buf.Append((char) ( '0' + ms/100));
                Append(buf, ms%100);
                buf.Append('Z');
            }

            return buf.ToString();
        }

        private static DateTime BuildDateTime(int year = 0, int month = 0, int day = 0, int hour = 0, int minute = 0,
            int second = 0, int millisecond = 0, DateTimeKind kind = DateTimeKind.Unspecified)
        {
            return new DateTime(year, month, day, hour, minute, second, millisecond, kind);
        }

        public static DateTime StringToDate(string text, int type)
        {
            int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0, millisecond = 0;

            if ((type & Date) != 0)
            {
                year = int.Parse(text.Substring(0, 4));
                month = int.Parse(text.Substring(5, 2));
                day = int.Parse(text.Substring(8, 2));

                if (type != DateTime || text.Length < 11)
                    return BuildDateTime(year, month, day, hour, minute, second, millisecond, DateTimeKind.Local);

                text = text.Substring(11);
            }

            hour = int.Parse(text.Substring(0, 2));
            minute = int.Parse(text.Substring(3, 2));
            second = int.Parse(text.Substring(6, 2));

            var pos = 8;
            if (pos < text.Length && text[pos] == '.')
            {
                var ms = 0;
                var f = 100;

                while (true)
                {
                    var d = text[++pos];
                    if (d < '0' || d > '9')
                        break;
                    ms += (d - '0')*f;
                    f /= 10;
                }
                millisecond = ms;
            }
            else
                millisecond = 0;

            var kind = DateTimeKind.Local;

            if (pos < text.Length)
            {
                if (text[pos] == '+' || text[pos] == '-')
                {
                    // Doesn't support GMT timezone. 
                    var c = BuildDateTime(year, month, day, hour, minute, second, millisecond, DateTimeKind.Utc);

                    return c;
                }
                if (text[pos] == 'Z')
                    kind = DateTimeKind.Utc;
                else
                    throw new Exception("Illegal time format!");
            }

            return BuildDateTime(year, month, day, hour, minute, second, millisecond, kind);
        }
    }
}
