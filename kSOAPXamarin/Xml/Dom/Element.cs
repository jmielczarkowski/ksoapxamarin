﻿using System;
using System.Collections.Generic;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Xml.Dom
{
    public class Element : Node
    {
        // ReSharper disable once InconsistentNaming
        protected internal string _Namespace;
        protected internal string Name;
        protected internal List<object> Attributes;
        protected internal Node Parent;
        protected internal List<object> Prefixes;

        /// <summary>
        /// Called when all properties are set, but before children
        /// are parsed. Please do not use SetParent for initialization code any longer.
        /// </summary>
        public void Init()
        {
        }

        /// <summary>
        /// Removes all children and attributes;
        /// </summary>
        public void Clear()
        {
            Attributes = null;
            Children = null;
        }

        /// <summary>
        /// Forwards creation request to parent if any, otherwise calls super.CreateElement.
        /// </summary>
        public override Element CreateElement(string namespace_,string name)
        {
            return Parent == null ? base.CreateElement(namespace_, name): Parent.CreateElement(namespace_, name);
        }

        /// <summary>
        /// Returns the number of attributes of this element.
        /// </summary>
        public int GetAttributeCount()
        {
            return Attributes?.Count ?? 0;
        }

        public string GetAttributeNamespace(int index)
        {
            return ((string[]) Attributes[index])[0];
        }

        public string GetAttributeName(int index)
        {
            return ((string[]) Attributes[index])[1];
        }

        public string GetAttributeValue(int index)
        {
            return ((string[]) Attributes[index])[2];
        }

        public string GetAttributeValue(string namespace_, string name)
        {
            for (var i = 0; i < GetAttributeCount(); i++)
            {
                if (name.Equals(GetAttributeName(i)) && (namespace_ == null || namespace_.Equals(GetAttributeNamespace(i))))
                    return GetAttributeValue(i);
            }
            return null;
        }

        /// <summary>
        /// Returns the root node, determined by ascending to the all parents un of the root element.
        /// </summary>
        public Node GetRoot()
        {
            var current = this;

            while (current.Parent != null)
            {
                if (!(current.Parent is Element))
                    return current.Parent;

                current = (Element) current.Parent;
            }

            return current;
        }

        /// <summary>
        /// Returns the (local) name of the element.
        /// </summary>
        public string GetName() => Name;

        /// <summary>
        /// Returns the namespace of the element.
        /// </summary>
        public string GetNamespace() => _Namespace;

        /// <summary>
        /// Returns the namespace for the given prefix.
        /// </summary>
        public string GetNamespaceUri(string prefix)
        {
            var cnt = GetNamespaceCount();
            for (var i = 0; i < cnt; i++)
            {
                if (prefix == GetNamespacePrefix(i) || prefix != null && prefix.Equals(GetNamespacePrefix(i)))
                    return GetNamespaceUri(i);
            }
            return Parent is Element element ? element.GetNamespaceUri(prefix): null;
        }

        /// <summary>
        /// Returns the number of declared namespaces, NOT including parent elements.
        /// </summary>
        public int GetNamespaceCount()
        {
            return Prefixes?.Count ?? 0;
        }

        public string GetNamespacePrefix(int i) => ((string[]) Prefixes[i])[0];

        public string GetNamespaceUri(int i) => ((string[]) Prefixes[i])[1];

        /// <summary>
        /// Returns the parent node of this element.
        /// </summary>
        public Node GetParent() => Parent;

        /// <summary>
        /// Builds the child elements from the given Parser. By overwriting 
        /// Parse, an element can take complete control over parsing its subtree.
        /// </summary>
        public override void Parse(XmlPullParser parser)
        {
            for (var i = parser.GetNamespaceCount(parser.GetDepth() - 1); i < parser.GetNamespaceCount(parser.GetDepth()); i++)
                SetPrefix(parser.GetNamespacePrefix(i), parser.GetNamespaceUri(i));


            for (var i = 0; i < parser.GetAttributeCount(); i++)
                SetAttribute(parser.GetAttributeNamespace(i), parser.GetAttributeName(i), parser.GetAttributeValue(i));

            Init();

            if (parser.IsEmptyElementTag())
                parser.NextToken();
            else
            {
                parser.NextToken();
                base.Parse(parser);

                if (GetChildCount() == 0)
                    AddChild(IgnorableWhitespace, "");
            }

            parser.Require( XmlPullParser.EndTag, GetNamespace(), GetName());
            parser.NextToken();
        }

        /// <summary>
        /// Sets the given Attribute; a value of null removes the Attribute.
        /// </summary>
        public void SetAttribute(string namespace_, string name, string value)
        {
            if (Attributes == null)
                Attributes = new List<object>();

            if (namespace_ == null) namespace_ = "";

            for (var i = Attributes.Count - 1; i >= 0; i--)
            {
                var attribut = (string[]) Attributes[i];
                if (!attribut[0].Equals(namespace_) || !attribut[1].Equals(name)) continue;
                if (value == null)
                    Attributes.RemoveAt(i);
                else
                    attribut[2] = value;
                return;
            }

            Attributes.Add(new {namespace_, name, value});
        }

        /// <summary>
        /// Sets the given prefix, a namespace value of null removes the prefix.
        /// </summary>
        public void SetPrefix(string prefix, string _namespace)
        {
            if (Prefixes == null)
                Prefixes = new List<object>();

            Prefixes.Add(new { prefix, _namespace });
        }

        /// <summary>
        /// Sets the name of the element.
        /// </summary>
        public void SetName(string name) => Name = name;

        /// <summary>
        /// sets the namespace of the element. Please note: For no
        /// * namespace, please use Xml.NO_NAMESPACE, null is not a legal
        /// * value. Currently, null is converted to Xml.NO_NAMESPACE, but
        /// * future versions may throw an exception.
        /// </summary>
        public void SetNamespace(string _namespace) => _Namespace = _namespace ?? throw new NotSupportedException("Use \"\" for empty namespace");

        /// <summary>
        /// Sets the Parent of this element. Automatically called from the add method.
        /// </summary>
        protected internal void SetParent(Node parent) => Parent = parent;

        /// <summary>
        /// Writes this element and all children to the given XmlWriter.
        /// </summary>
        public override void Write(IXmlSerializer writer)
        {
            if (Prefixes != null)
            {
                for (var i = 0; i < Prefixes.Count; i++)
                    writer.SetPrefix(GetNamespacePrefix(i), GetNamespaceUri(i));
            }

            writer.StartTag(GetNamespace(),GetName());

            var len = GetAttributeCount();

            for (var i = 0; i < len; i++)
            {
                writer.Attribute(GetAttributeNamespace(i), GetAttributeName(i), GetAttributeValue(i));
            }

            WriteChildren(writer);
            writer.EndTag(GetNamespace(), GetName());
        }
    }
}
