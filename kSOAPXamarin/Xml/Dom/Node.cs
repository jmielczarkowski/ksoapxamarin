﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Xml.Dom
{
    public class Node
    {
        public const int Document = 0;
        public const int Element = 2;
        public const int Text = 4;
        public const int CdSect = 5;
        public const int EntityRef = 6;
        public const int IgnorableWhitespace = 7;
        public const int ProcessingInstruction = 8;
        public const int Comment = 9;
        public const int DocDecl = 10;

        protected internal List<object> Children;
        protected internal StringBuilder Types;

        /// <summary>
        /// inserts the given child object of the given type at the given index.
        /// </summary>
        public void AddChild(int index, int type, object child)
        {
            if (child == null)
                throw new NullReferenceException();

            if (Children == null)
            {
                Children = new List<object>();
                Types = new StringBuilder();
            }

            if (type == Element)
            {
                if (!(child is Element))
                    throw new Exception("Element obj expected");

                ((Element) child).SetParent(this);
            }
            else if (!(child is string))
                throw new Exception("String expected");

            Children.Insert(index, child);
            Types.Insert(index, (char) type);
        }

        /// <summary>
        /// Convenience method for AddChild (GetChildCount (), child).
        /// </summary>
        public void AddChild(int type, object child)
        {
            AddChild(GetChildCount(), type, child);
        }

        /// <summary>
        /// Builds a default element with the given properties. Elements
        /// should always be created using this method instead of the
        /// constructor in order to enable construction of specialized
        /// subclasses by deriving custom Document classes. Please note:
        /// For no namespace, please use Xml.NO_NAMESPACE, null is not a
        /// legal value. Currently, null is converted to Xml.NO_NAMESPACE,
        /// but future versions may throw an exception.
        /// </summary>
        public virtual Element CreateElement(string namespace_, string name)
        {
            return new Element { _Namespace = namespace_ ?? "", Name = name };
        }

        /// <summary>
        /// Returns the child object at the given index. For child
        /// elements, an Element object is returned. For all other child
        /// types, a String is returned.
        /// </summary>
        public object GetChild(int index)
        {
            return Children.ElementAt(index);
        }

        /// <summary>
        /// Returns the number of child objects.
        /// </summary>
        public int GetChildCount()
        {
            return Children?.Count ?? 0;
        }

        /// <summary>
        /// Returns the element at the given index. If the node at the
        /// given index is a Text node, null is returned.
        /// </summary>
        public Element GetElement(int index)
        {
            return GetChild(index) is Element element ? element : null;
        }

        /// <summary>
        /// Returns the element with the given namespace and name. If the
        /// element is not found, or more than one matching elements are
        /// found, an exception is thrown.
        /// </summary>
        public Element GetElement(string namespace_, string name)
        {
            var i = IndexOf(namespace_, name, 0);
            var j = IndexOf(namespace_, name, i + 1);

            if (i == -1 || j != -1)
                throw new Exception($"Element {namespace_}{name} not found.");

            return GetElement(i);
        }

        /// <summary>
        /// Returns the Text node with the given index or null if the node
        /// with the given index is not a Text node.
        /// </summary>
        public string GetText(int index)
        {
            return IsText(index) ? (string) GetChild(index) : null;
        }

        /// <summary>
        /// Returns the type of the child at the given index. Possible 
        /// types are ELEMENT, TEXT, COMMENT, and PROCESSING_INSTRUCTION.
        /// </summary>
        public int GetType(int index)
        {
            return Types[index];
        }

        /// <summary>
        /// Performs search for an element with the given namespace and
        /// name, starting at the given start index. A null namespace
        /// matches any namespace, please use Xml.NO_NAMESPACE for no
        /// namespace).  returns -1 if no matching element was found.
        /// </summary>
        public int IndexOf(string namespace_, string name, int startIndex)
        {

            var len = GetChildCount();

            for (var i = startIndex; i < len; i++)
            {

                var child = GetElement(i);

                if (child != null && name.Equals(child.GetName()) 
                                  && (namespace_ == null || namespace_.Equals(child.GetNamespace())))
                    return i;
            }
            return -1;
        }

        public bool IsText(int i)
        {
            var t = GetType(i);
            return t == Text || t == IgnorableWhitespace || t == CdSect;
        }

        /// <summary>
        /// Recursively builds the child elements from the given parser
        /// until an end tag or end document is found. 
        /// The end tag is not consumed.
        /// </summary>
        public virtual void Parse(XmlPullParser parser)
        {
            var leave = false;

            do
            {
                var type = parser.GetEventType();

                switch (type)
                {

                    case XmlPullParser.StartTag:
                    {
                        Element child = CreateElement(parser.GetNamespace(), parser.GetName());
                        AddChild(Element, child);
                        child.Parse(parser);
                        break;
                    }

                    case XmlPullParser.EndDocument:
                    case XmlPullParser.EndTag:
                        leave = true;
                        break;

                    default:
                        if (parser.GetText() != null)
                            AddChild(type == XmlPullParser.EntityRef ? Text : type, parser.GetText());
                        else if (type == XmlPullParser.EntityRef && parser.GetName() != null)
                            AddChild(EntityRef, parser.GetName());

                        parser.NextToken();
                        break;
                }
            } while (!leave);
        }

        /// <summary>
        /// Removes the child object at the given index.
        /// </summary>
        public void RemoveChild(int idx)
        {
            Children.RemoveAt(idx);

            var n = Types.Length - 1;
            for (var i = idx; i < n; i++)
                Types[i] = Types[i + 1];

            Types.Length = n;
        }

        /// <summary>
        /// Writes this node to the given XmlWriter. For node and document,
        /// this method is identical to WriteChildren, except that the
        /// stream is flushed automatically.
        /// </summary>
        public virtual void Write(IXmlSerializer writer)
        {
            WriteChildren(writer);
            writer.Flush();
        }

        /// <summary>
        /// Writes the children of this node to the given XmlWriter.
        /// </summary>
        public void WriteChildren(IXmlSerializer writer)
        {
            if (Children == null)
                return;

            var len = Children.Count;

            for (var i = 0; i < len; i++)
            {
                var type = GetType(i);
                var child = Children[i];
                switch (type)
                {
                    case Element:
                        ((Element) child).Write(writer);
                        break;

                    case Text:
                        writer.Text((string) child);
                        break;

                    case IgnorableWhitespace:
                        writer.IgnorableWhitespace((string) child);
                        break;

                    case CdSect:
                        writer.CdSect((string) child);
                        break;

                    case Comment:
                        writer.Comment((string) child);
                        break;

                    case EntityRef:
                        writer.EntityRef((string) child);
                        break;

                    case ProcessingInstruction:
                        writer.ProcessingInstruction((string) child);
                        break;

                    case DocDecl:
                        writer.DoCdecl((string) child);
                        break;

                    default:
                        throw new Exception($"Illegal type: {type}");
                }
            }

        }
    }

}
