﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Xml.IO
{
    public class FXmlParser : XmlPullParser
    {
        private object _location;
        private static readonly string UnexpectedEof = "Unexpected EOF";
        private static readonly string IllegalType = "Wrong event type";
        private const int Legacy = 999;
        private const int XmlDecl = 998;
        private string _version;
        private bool? _standalone;
        private bool _processNsp;
        private bool _relaxed;
        private Dictionary<object, object> _entityMap;
        private int _depth;
        private string[] _elementStack = new string[16];
        private string[] _nspStack = new string[8];
        private int[] _nspCounts = new int[4];
        private TextReader _reader;
        private string _encoding;
        private readonly char[] _srcBuf;
        private int _srcPos;
        private int _srcCount;
        private int _line;
        private int _column;

        /// <summary>
        /// Target buffer for storing incoming Text (including aggregated resolved entities).
        /// </summary>
        private char[] _txtBuf = new char[128];

        /// <summary>
        /// Write position.
        /// </summary>
        private int _txtPos;

        private int _type;
        private bool _isWhitespace;
        private string _namespace;
        private string _prefix;
        private string _name;

        private bool _degenerated;
        private int _attributeCount;
        private string[] _attributes = new string[16];
        private string _error;

        /// <summary>
        /// A separate Peek buffer seems simpler than managing wrap around in the first level Read buffer.
        /// </summary>
        private readonly int[] _peek = new int[2];
        private int _peekCount;
        private bool _wasCr;

        private bool _unresolved;
        private bool _token;

        public FXmlParser()
        {
            _srcBuf = new char[8192];
        }

        private bool IsProp(string n1, bool prop, string n2)
        {
            if (!n1.StartsWith("http://xmlpull.org/v1/doc/"))
                return false;

            return prop ? n1.Substring(42).Equals(n2) : n1.Substring(40).Equals(n2);
        }

        private void AdjustNsp()
        {
            var any = false;
            int cut;

            for (var i = 0; i < _attributeCount << 2; i += 4)
            {
                var attrName = _attributes[i + 2];
                cut = attrName.IndexOf(':');
                string prefix;

                if (cut != -1)
                {
                    prefix = attrName.Substring(0, cut);
                    attrName = attrName.Substring(cut + 1);
                }
                else if (attrName.Equals("xmlns"))
                {
                    prefix = attrName;
                    attrName = null;
                }
                else
                    continue;

                if (!prefix.Equals("xmlns"))
                {
                    any = true;
                }
                else
                {
                    var j = (_nspCounts[_depth]++) << 1;

                    _nspStack = EnsureCapacity(_nspStack, j + 2);
                    _nspStack[j] = attrName;
                    _nspStack[j + 1] = _attributes[i + 3];

                    if (attrName != null && _attributes[i + 3].Equals(""))
                        Error("Illegal empty namespace.");

                    Array.Copy(_attributes, i + 4, _attributes, i, (--_attributeCount << 2) - i);
                    i -= 4;
                }
            }

            if (any)
            {
                for (var i = (_attributeCount << 2) - 4; i >= 0; i -= 4)
                {

                    var attrName = _attributes[i + 2];
                    cut = attrName.IndexOf(':');

                    if (cut == 0 && !_relaxed)
                        throw new Exception($"Illegal Attribute name: {attrName} at {this}.");

                    if (cut != -1)
                    {
                        var attrPrefix = attrName.Substring(0, cut);

                        attrName = attrName.Substring(cut + 1);

                        var attrNs = GetNamespace(attrPrefix);

                        if (attrNs == null && !_relaxed)
                            throw new Exception($"Undefined Prefix: {attrPrefix} in {this}");

                        _attributes[i] = attrNs;
                        _attributes[i + 1] = attrPrefix;
                        _attributes[i + 2] = attrName;
                    }
                }
            }

            cut = _name.IndexOf(':');

            if (cut == 0)
                Error($"Illegal tag name: {_name}.");

            if (cut != -1)
            {
                _prefix = _name.Substring(0, cut);
                _name = _name.Substring(cut + 1);
            }

            _namespace = GetNamespace(_prefix);

            if (_namespace == null)
            {
                if (_prefix != null)
                    Error($"Undefined prefix: {_prefix}");
                _namespace = NoNamespace;
            }
        }

        private string[] EnsureCapacity(string[] arr, int required)
        {
            if (arr.Length >= required)
                return arr;

            var bigger = new string[required + 16];
            Array.Copy(arr, 0, bigger, 0, arr.Length);
            return bigger;
        }

        private void Error(string desc)
        {
            if (_relaxed)
            {
                if (_error == null)
                    _error = $"ERR: {desc}";
            }
            else
                Exception(desc);
        }

        private void Exception(string desc)
        {
            throw new XmlPullParserException(desc.Length < 100 ? desc : desc.Substring(0, 100) + "\n", this, null);
        }

        /// <summary>
        /// Common base for Next and NextToken. Clears the state, except from txtPos and whitespace. Does not set the type variable.
        /// </summary>
        private void NextImpl()
        {
            if (_reader == null)
                Exception("No Input specified.");

            if (_type == EndTag)
                _depth--;

            while (true)
            {
                _attributeCount = -1;

                if (_degenerated)
                {
                    _degenerated = false;
                    _type = EndTag;
                    return;
                }

                if (_error != null)
                {
                    foreach (var err in _error)
                        Push(err);

                    _error = null;
                    _type = Comment;
                    return;
                }

                _prefix = null;
                _name = null;
                _namespace = null;
                _type = PeekType();

                switch (_type)
                {
                    case EntityRef:
                        PushEntity();
                        return;
                    case StartTag:
                        ParseStartTag(false);
                        return;
                    case EndTag:
                        ParseEndTag();
                        return;
                    case EndDocument:
                        return;
                    case Text:
                        PushText('<', !_token);
                        if (_depth != 0)
                            return;
                        if (_isWhitespace)
                            _type = IgnorableWhitespace;
                        return;
                    default:
                        _type = ParseLegacy(_token);
                        if (_type != XmlDecl)
                            return;
                        break;
                }
            }
        }

        private int ParseLegacy(bool hasPush)
        {
            var req = "";
            int term;
            int result;
            var prev = 0;

            Read();
            var c = Read();

            if (c == '?')
            {
                if ((Peek(0) == 'x' || Peek(0) == 'X')
                    && (Peek(1) == 'm' || Peek(1) == 'M'))
                {

                    if (hasPush)
                    {
                        Push(Peek(0));
                        Push(Peek(1));
                    }
                    Read();
                    Read();

                    if ((Peek(0) == 'l' || Peek(0) == 'L') && Peek(1) <= ' ')
                    {

                        if (_line != 1 || _column > 4)
                            Error("Must not start with xml.");

                        ParseStartTag(true);

                        if (_attributeCount < 1 || !"version".Equals(_attributes[2]))
                            Error("Version expected.");

                        _version = _attributes[3];

                        var pos = 1;

                        if (pos < _attributeCount
                            && "encoding".Equals(_attributes[2 + 4]))
                        {
                            _encoding = _attributes[3 + 4];
                            pos++;
                        }

                        if (pos < _attributeCount && "standalone".Equals(_attributes[4*pos + 2]))
                        {
                            var st = _attributes[3 + 4*pos];
                            switch (st)
                            {
                                case "yes":
                                    _standalone = true;
                                    break;
                                case "no":
                                    _standalone = false;
                                    break;
                                default:
                                    Error($"Illegal standalone value: {st}.");
                                    break;
                            }
                            pos++;
                        }

                        if (pos != _attributeCount)
                            Error("Illegal xmlDecl");

                        _isWhitespace = true;
                        _txtPos = 0;

                        return XmlDecl;
                    }
                }

                term = '?';
                result = ProcessingInstruction;
            }
            else if (c == '!')
            {
                if (Peek(0) == '-')
                {
                    result = Comment;
                    req = "--";
                    term = '-';
                }
                else if (Peek(0) == '[')
                {
                    result = CdSect;
                    req = "[CDATA[";
                    term = ']';
                    hasPush = true;
                }
                else
                {
                    result = DocDecl;
                    req = "DOCTYPE";
                    term = -1;
                }
            }
            else
            {
                Error($"Illegal: <{c}");
                return Comment;
            }

            foreach (var request in req)
                Read(request);

            if (result == DocDecl)
                ParseDoctype(hasPush);
            else
            {
                while (true)
                {
                    c = Read();
                    if (c == -1)
                    {
                        Error(UnexpectedEof);
                        return Comment;
                    }

                    if (hasPush)
                        Push(c);

                    if ((term == '?' || c == term)
                        && Peek(0) == term
                        && Peek(1) == '>')
                        break;

                    prev = c;
                }

                if (term == '-' && prev == '-' && !_relaxed)
                    Error("Illegal Comment delimiter: --->");

                Read();
                Read();

                if (hasPush && term != '?')
                    _txtPos--;

            }
            return result;
        }

        private void ParseDoctype(bool hasPush)
        {
            var nesting = 1;
            var quoted = false;

            while (true)
            {
                var i = Read();
                switch (i)
                {
                    case -1:
                        Error(UnexpectedEof);
                        return;
                    case '\'':
                        quoted = !quoted;
                        break;
                    case '<':
                        if (!quoted)
                            nesting++;
                        break;
                    case '>':
                        if (!quoted)
                        {
                            if ((--nesting) == 0)
                                return;
                        }
                        break;
                }
                if (hasPush)
                    Push(i);
            }
        }

        private void ParseEndTag()
        {
            Read(); // '<'
            Read(); // '/'
            _name = ReadName();
            Skip();
            Read('>');

            var sp = (_depth - 1) << 2;

            if (_depth == 0)
            {
                Error("Element stack empty.");
                _type = Comment;
                return;
            }

            if (!_relaxed)
            {
                if (!_name.Equals(_elementStack[sp + 3]))
                    Error($"expected: {_elementStack[sp + 3]}, Read: {_name}");

                _namespace = _elementStack[sp];
                _prefix = _elementStack[sp + 1];
                _name = _elementStack[sp + 2];
            }
        }

        private int PeekType()
        {
            switch (Peek(0))
            {
                case -1:
                    return EndDocument;
                case '&':
                    return EntityRef;
                case '<':
                    switch (Peek(1))
                    {
                        case '/':
                            return EndTag;
                        case '?':
                        case '!':
                            return Legacy;
                        default:
                            return StartTag;
                    }
                default:
                    return Text;
            }
        }

        private string Get(int pos) => new string(_txtBuf, pos, _txtPos - pos);

        private void Push(int c)
        {
            _isWhitespace &= c <= ' ';

            if (_txtPos == _txtBuf.Length)
            {
                var bigger = new char[_txtPos*4/3 + 4];
                Array.Copy(_txtBuf, 0, bigger, 0, _txtPos);
                _txtBuf = bigger;
            }

            _txtBuf[_txtPos++] = (char) c;
        }

        /// <summary>
        /// Sets name and attributes.
        /// </summary>
        private void ParseStartTag(bool xmlDecl)
        {
            if (!xmlDecl)
                Read();

            _name = ReadName();
            _attributeCount = 0;

            while (true)
            {
                Skip();

                var c = Peek(0);

                if (xmlDecl)
                {
                    if (c == '?')
                    {
                        Read();
                        Read('>');
                        return;
                    }
                }
                else
                {
                    if (c == '/')
                    {
                        _degenerated = true;
                        Read();
                        Skip();
                        Read('>');
                        break;
                    }

                    // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                    if (c == '>' && !xmlDecl)
                    {
                        Read();
                        break;
                    }
                }

                if (c == -1)
                {
                    Error(UnexpectedEof);
                    return;
                }

                var attrName = ReadName();

                if (attrName.Length == 0)
                {
                    Error("attr name expected");
                    break;
                }

                var i = (_attributeCount++) << 2;

                _attributes = EnsureCapacity(_attributes, i + 4);

                _attributes[i++] = "";
                _attributes[i++] = null;
                _attributes[i++] = attrName;

                Skip();

                if (Peek(0) != '=')
                {
                    if (!_relaxed)
                    {
                        Error("Attr.value missing f. " + attrName);
                    }
                    _attributes[i] = attrName;
                }
                else
                {
                    Read('=');
                    Skip();
                    var delimiter = Peek(0);

                    if (delimiter != '\'' && delimiter != '"')
                    {
                        if (!_relaxed)
                        {
                            Error("attr value delimiter missing!");
                        }
                        delimiter = ' ';
                    }
                    else
                        Read();

                    var p = _txtPos;
                    PushText(delimiter, true);

                    _attributes[i] = Get(p);
                    _txtPos = p;

                    if (delimiter != ' ')
                        Read();
                }
            }

            var sp = _depth++ << 2;

            _elementStack = EnsureCapacity(_elementStack, sp + 4);
            _elementStack[sp + 3] = _name;

            if (_depth >= _nspCounts.Length)
            {
                var bigger = new int[_depth + 4];
                Array.Copy(_nspCounts, 0, bigger, 0, _nspCounts.Length);
                _nspCounts = bigger;
            }

            _nspCounts[_depth] = _nspCounts[_depth - 1];

            if (_processNsp)
                AdjustNsp();
            else
                _namespace = "";

            _elementStack[sp] = _namespace;
            _elementStack[sp + 1] = _prefix;
            _elementStack[sp + 2] = _name;
        }

        private void PushEntity()
        {
            Push(Read());
            var pos = _txtPos;

            while (true)
            {
                var c = Peek(0);
                if (c == ';')
                {
                    Read();
                    break;
                }
                if (c < 128
                    && (c < '0' || c > '9')
                    && (c < 'a' || c > 'z')
                    && (c < 'A' || c > 'Z')
                    && c != '_'
                    && c != '-'
                    && c != '#')
                {
                    if (!_relaxed)
                    {
                        Error("unterminated entity ref");
                    }

                    return;
                }

                Push(Read());
            }

            var code = Get(pos);
            _txtPos = pos - 1;
            if (_token && _type == EntityRef)
            {
                _name = code;
            }

            if (code[0] == '#')
            {
                var c = code[1] == 'x'
                        ? int.Parse(code.Substring(2), NumberStyles.HexNumber)
                        : int.Parse(code.Substring(2), NumberStyles.Number);
                Push(c);
                return;
            }

            var result = (string) _entityMap[code];

            _unresolved = result == null;

            if (_unresolved)
            {
                if (!_token)
                    Error("unresolved: &" + code + ";");
            }
            else
            {
                // ReSharper disable once PossibleNullReferenceException
                foreach (var str in result)
                    Push(str);
            }
        }

        private void PushText(int delimiter, bool resolveEntities)
        {
            var next = Peek(0);
            var cbrCount = 0;

            while (next != -1 && next != delimiter)
            {
                if (delimiter == ' ')
                    if (next <= ' ' || next == '>')
                        break;

                if (next == '&')
                {
                    if (!resolveEntities)
                        break;

                    PushEntity();
                }
                else if (next == '\n' && _type == StartTag)
                {
                    Read();
                    Push(' ');
                }
                else
                    Push(Read());

                if (next == '>' && cbrCount >= 2 && delimiter != ']')
                    Error("Illegal: ]]>");

                if (next == ']')
                    cbrCount++;
                else
                    cbrCount = 0;

                next = Peek(0);
            }
        }

        private void Read(char c)
        {
            var a = Read();
            if (a != c)
                Error($"Expected: {c}, actual: {(char) a}.");
        }

        private int Read()
        {
            int result;

            if (_peekCount == 0)
                result = Peek(0);
            else
            {
                result = _peek[0];
                _peek[0] = _peek[1];
            }
            _peekCount--;

            _column++;

            if (result != '\n')
                return result;

            _line++;
            _column = 1;

            return result;
        }

        private int Peek(int pos)
        {

            while (pos >= _peekCount)
            {

                int nw;

                if (_srcBuf.Length <= 1)
                    nw = _reader.Read();
                else if (_srcPos < _srcCount)
                    nw = _srcBuf[_srcPos++];
                else
                {
                    _srcCount = _reader.Read(_srcBuf, 0, _srcBuf.Length);
                    if (_srcCount <= 0)
                        nw = -1;
                    else
                        nw = _srcBuf[0];

                    _srcPos = 1;
                }

                if (nw == '\r')
                {
                    _wasCr = true;
                    _peek[_peekCount++] = '\n';
                }
                else
                {
                    if (nw == '\n')
                    {
                        if (!_wasCr)
                            _peek[_peekCount++] = '\n';
                    }
                    else
                        _peek[_peekCount++] = nw;

                    _wasCr = false;
                }
            }

            return _peek[pos];
        }

        private string ReadName()
        {
            var pos = _txtPos;
            var c = Peek(0);
            if ((c < 'a' || c > 'z')
                && (c < 'A' || c > 'Z')
                && c != '_'
                && c != ':'
                && c < 0x0c0
                && !_relaxed)
                Error("name expected");

            do
            {
                Push(Read());
                c = Peek(0);
            } while ((c >= 'a' && c <= 'z')
                     || (c >= 'A' && c <= 'Z')
                     || (c >= '0' && c <= '9')
                     || c == '_'
                     || c == '-'
                     || c == ':'
                     || c == '.'
                     || c >= 0x0b7);

            var result = Get(pos);
            _txtPos = pos;
            return result;
        }

        private void Skip()
        {
            while (true)
            {
                var c = Peek(0);
                if (c > ' ' || c == -1)
                    break;
                Read();
            }
        }

        public override void SetFeature(string feature, bool value)
        {
            if (FeatureProcessNamespaces.Equals(feature))
                _processNsp = value;
            else if (IsProp(feature, false, "relaxed"))
                _relaxed = value;
            else
                Exception($"Unsupported feature: {feature}.");
        }

        public override bool GetFeature(string feature)
        {
            if (FeatureProcessNamespaces.Equals(feature))
                return _processNsp;

            if (IsProp(feature, false, "relaxed"))
                return _relaxed;

            return false;
        }

        public override void SetProperty(string property, object value)
        {
            if (IsProp(property, true, "location"))
                _location = value;
            else
                throw new XmlPullParserException("unsupported property: " + property);
        }

        public override object GetProperty(string property)
        {
            if (IsProp(property, true, "xmldecl-version"))
                return _version;
            if (IsProp(property, true, "xmldecl-standalone"))
                return _standalone;
            if (IsProp(property, true, "location"))
                return _location ?? _reader.ToString();
            return null;
        }

        public override void SetInput(TextReader reader)
        {
            _reader = reader;
            _line = 1;
            _column = 0;
            _type = StartDocument;
            _name = null;
            _namespace = null;
            _degenerated = false;
            _attributeCount = -1;
            _encoding = null;
            _version = null;
            _standalone = null;

            if (reader == null)
                return;

            _srcPos = 0;
            _srcCount = 0;
            _peekCount = 0;
            _depth = 0;

            _entityMap = new Dictionary<object, object>
            {
                ["amp"] = "&",
                ["apos"] = "'",
                ["gt"] = ">",
                ["lt"] = "<",
                ["quot"] = "\""
            };
        }

        public override void SetInput(Stream inputStream, string encoding)
        {
            _srcPos = 0;
            _srcCount = 0;
            var enc = encoding;

            if (inputStream == null)
                throw new ArgumentException();

            try
            {

                if (enc == null)
                {
                    long chk = 0;

                    while (_srcCount < 4)
                    {
                        var i = inputStream.ReadByte();
                        if (i == -1)
                            break;
                        chk = (int)(chk << 8) | i;
                        _srcBuf[_srcCount++] = (char) i;
                    }

                    if (_srcCount == 4)
                    {
                        switch (chk)
                        {
                            case 0x00000FEFF:
                                enc = "UTF-32BE";
                                _srcCount = 0;
                                break;

                            case 0x0FFFE0000:
                                enc = "UTF-32LE";
                                _srcCount = 0;
                                break;

                            case 0x03c:
                                enc = "UTF-32BE";
                                _srcBuf[0] = '<';
                                _srcCount = 1;
                                break;

                            case 0x03c000000:
                                enc = "UTF-32LE";
                                _srcBuf[0] = '<';
                                _srcCount = 1;
                                break;

                            case 0x0003c003f:
                                enc = "UTF-16BE";
                                _srcBuf[0] = '<';
                                _srcBuf[1] = '?';
                                _srcCount = 2;
                                break;

                            case 0x03c003f00:
                                enc = "UTF-16LE";
                                _srcBuf[0] = '<';
                                _srcBuf[1] = '?';
                                _srcCount = 2;
                                break;

                            case 0x03c3f786d:
                                while (true)
                                {
                                    var i = inputStream.ReadByte();
                                    if (i == -1)
                                        break;
                                    _srcBuf[_srcCount++] = (char) i;
                                    if (i == '>')
                                    {
                                        var s = new string(_srcBuf, 0, _srcCount);
                                        // ReSharper disable once StringIndexOfIsCultureSpecific.1
                                        var i0 = s.IndexOf("encoding");
                                        if (i0 != -1)
                                        {
                                            while (s[i0] != '"'
                                                   && s[i0] != '\'')
                                                i0++;
                                            char deli = s[i0++];
                                            var i1 = s.IndexOf(deli, i0);
                                            enc = s.Substring(i0, i1 - i0);
                                        }
                                        break;
                                    }
                                }
                                break;
                            default:
                                if ((chk & 0x0ffff0000) == 0x0FEFF0000)
                                {
                                    enc = "UTF-16BE";
                                    _srcBuf[0] =
                                        (char) ((_srcBuf[2] << 8) | _srcBuf[3]);
                                    _srcCount = 1;
                                }
                                else if ((chk & 0x0ffff0000) == 0x0fffe0000)
                                {
                                    enc = "UTF-16LE";
                                    _srcBuf[0] =
                                        (char) ((_srcBuf[3] << 8) | _srcBuf[2]);
                                    _srcCount = 1;
                                }
                                else if ((chk & 0x0ffffff00) == 0x0EFBBBF00)
                                {
                                    enc = "UTF-8";
                                    _srcBuf[0] = _srcBuf[3];
                                    _srcCount = 1;
                                }
                                break;
                        }
                    }
                }

                if (enc == null)
                    enc = "UTF-8";

                var sc = _srcCount;
                SetInput(new StreamReader(inputStream, Encoding.GetEncoding(enc)));
                _encoding = encoding;
                _srcCount = sc;
            }
            catch (Exception exception)
            {
                throw new XmlPullParserException($"Invalid stream or encoding: {exception}", this, exception);
            }
        }

        public override string GetInputEncoding() => _encoding;

        public override void DefineEntityReplacementText(string entity, string value)
        {
            if (_entityMap == null)
                throw new Exception("Entity replacement Text must be defined after SetInput!");

            _entityMap[entity] = value;
        }

        public override int GetNamespaceCount(int depth)
        {
            if (depth > _depth)
                throw new IndexOutOfRangeException();

            return _nspCounts[depth];
        }

        public override string GetNamespacePrefix(int pos) => _nspStack[pos << 1];

        public override string GetNamespaceUri(int pos) => _nspStack[(pos << 1) + 1];

        public override string GetNamespace(string prefix)
        {
            if ("xml".Equals(prefix))
                return "http://www.w3.org/XML/1998/namespace";

            if ("xmlns".Equals(prefix))
                return "http://www.w3.org/2000/xmlns/";

            for (var i = (GetNamespaceCount(_depth) << 1) - 2; i >= 0; i -= 2)
            {
                if (prefix == null)
                {
                    if (_nspStack[i] == null)
                        return _nspStack[i + 1];
                }

                else if (prefix.Equals(_nspStack[i]))
                    return _nspStack[i + 1];
            }
            return null;
        }

        public override int GetDepth() => _depth;

        public override string GetPositionDescription()
        {
            var buf = new StringBuilder(_type < Types.Length ? Types[_type] : "unknown");
            buf.Append(' ');

            if (_type == StartTag || _type == EndTag)
            {
                if (_degenerated)
                    buf.Append("(empty) ");
                buf.Append('<');
                if (_type == EndTag)
                    buf.Append('/');

                if (_prefix != null)
                    buf.Append("{" + _namespace + "}" + _prefix + ":")
                        ;
                buf.Append(_name);

                var cnt = _attributeCount << 2;
                for (var i = 0; i < cnt; i += 4)
                {
                    buf.Append(' ');
                    if (_attributes[i + 1] != null)
                        buf.Append("{" + _attributes[i] + "}" + _attributes[i + 1] + ":");

                    buf.Append(_attributes[i + 2] + "='" + _attributes[i + 3] + "'");
                }

                buf.Append('>');
            }
            else if (_type == IgnorableWhitespace) {}
            else if (_type != Text)
                buf.Append(GetText());
            else if (_isWhitespace)
                buf.Append("(whitespace)");
            else
            {
                var text = GetText();
                if (text.Length > 16)
                    text = text.Substring(0, 16) + "...";
                buf.Append(text);
            }

            buf.Append("@" + _line + ":" + _column);
            if (_location != null)
            {
                buf.Append(" in ");
                buf.Append(_location);
            }
            else if (_reader != null)
            {
                buf.Append(" in ");
                buf.Append(_reader);
            }
            return buf.ToString();
        }

        public override int GetLineNumber() => _line;

        public override int GetColumnNumber() => _column;

        public override bool IsWhitespace()
        {
            if (_type != Text && _type != IgnorableWhitespace && _type != CdSect)
                Exception(IllegalType);

            return _isWhitespace;
        }

        public override string GetText()
        {
            return _type < Text || (_type == EntityRef && _unresolved) ?
                null : Get(0);
        }

        public override char[] GetTextCharacters(int[] posLen)
        {
            if (_type >= Text)
            {
                if (_type == EntityRef)
                {
                    posLen[0] = 0;
                    posLen[1] = _name.Length;
                    return _name.ToCharArray();
                }
                posLen[0] = 0;
                posLen[1] = _txtPos;
                return _txtBuf;
            }

            posLen[0] = -1;
            posLen[1] = -1;
            return null;
        }

        public override string GetNamespace() => _namespace;

        public override string GetName() => _name;

        public override string GetPrefix() => _prefix;

        public override bool IsEmptyElementTag()
        {
            if (_type != StartTag)
                Exception(IllegalType);
            return _degenerated;
        }

        public override int GetAttributeCount() => _attributeCount;

        public override string GetAttributeNamespace(int index)
        {
            if (index >= _attributeCount)
                throw new IndexOutOfRangeException();

            return _attributes[index << 2];
        }

        public override string GetAttributeName(int index)
        {
            if (index >= _attributeCount)
                throw new IndexOutOfRangeException();
            return _attributes[(index << 2) + 2];
        }

        public override string GetAttributePrefix(int index)
        {
            if (index >= _attributeCount)
                throw new IndexOutOfRangeException();
            return _attributes[(index << 2) + 1];
        }

        public override string GetAttributeType(int index) => "CDATA";

        public override bool IsAttributeDefault(int index) => false;

        public override string GetAttributeValue(int index)
        {
            if (index >= _attributeCount)
                throw new IndexOutOfRangeException();
            return _attributes[(index << 2) + 3];
        }

        // ReSharper disable once ParameterHidesMember
        public override string GetAttributeValue(string _namespace, string name)
        {
            for (var i = (_attributeCount << 2) - 4; i >= 0; i -= 4)
            {
                if (_attributes[i + 2].Equals(name) 
                    && (_namespace == null || _attributes[i].Equals(_namespace)))
                    return _attributes[i + 3];
            }

            return null;
        }

        public override int GetEventType() => _type;

        public override int Next()
        {
            _txtPos = 0;
            _isWhitespace = true;
            var minType = 9999;
            _token = false;

            do
            {
                NextImpl();
                if (_type < minType)
                    minType = _type;
            }
            while (minType > EntityRef || (minType >= Text && PeekType() >= Text));

            _type = minType;
            if (_type > Text)
                _type = Text;

            return _type;
        }

        public override int NextToken()
        {
            _isWhitespace = true;
            _txtPos = 0;
            _token = true;

            NextImpl();
            return _type;
        }

        // ReSharper disable once ParameterHidesMember
        public override void Require(int type, string _namespace, string name)
        {
            if (type != _type 
                || _namespace != null && !_namespace.Equals(GetNamespace())
                || name != null && !name.Equals(GetName()))
                Exception($"Expected: {Types[type]}{_namespace}{name}.");
        }

        public override string NextText()
        {
            if (_type != StartTag)
                Exception("Precondition: START_TAG.");

            Next();

            string result;

            if (_type == Text)
            {
                result = GetText();
                Next();
            }
            else
                result = "";

            if (_type != EndTag)
                Exception("END_TAG expected.");

            return result;
        }

        public override int NextTag()
        {
            Next();
            if (_type == Text && _isWhitespace)
                Next();

            if (_type != EndTag && _type != StartTag)
                Exception("Unexpected type.");

            return _type;
        }

        public override void SkipSubTree()
        {
            Require(StartTag, null, null);
            var level = 1;
            while (level > 0)
            {
                var eventType = Next();
                switch (eventType)
                {
                    case EndTag:
                        --level;
                        break;
                    case StartTag:
                        ++level;
                        break;
                }
            }
        }
    }
}
