﻿using System;
using System.IO;
using System.Text;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Xml.IO
{
    public class FXmlSerializer : IXmlSerializer
    {
        private TextWriter _writer;
        private bool _pending;
        private int _auto;
        private int _depth;
        private string[] _elementStack = new string[12];
        private int[] _nspCounts = new int[4];
        private string[] _nspStack = new string[8];
        private bool[] _indent = new bool[4];
        private bool _unicode;
        private Encoding _encoding;

        private void Check(bool close)
        {
            if (!_pending)
                return;

            _depth++;
            _pending = false;

            if (_indent.Length <= _depth)
            {
                var hlp = new bool[_depth + 4];
                Array.Copy(_indent, 0, hlp, 0, _depth);
                _indent = hlp;
            }
            _indent[_depth] = _indent[_depth - 1];

            for (var i = _nspCounts[_depth - 1]; i < _nspCounts[_depth]; i++)
            {
                _writer.Write(' ');
                _writer.Write("xmlns");
                if (!"".Equals(_nspStack[i*2]))
                {
                    _writer.Write(':');
                    _writer.Write(_nspStack[i*2]);
                }
                else if ("".Equals(GetNamespace()) && !"".Equals(_nspStack[i*2 + 1]))
                    throw new InvalidOperationException("Cannot set default namespace for elements in no namespace");
                _writer.Write("=\"");
                WriteEscaped(_nspStack[i*2 + 1], '"');
                _writer.Write('"');
            }

            if (_nspCounts.Length <= _depth + 1)
            {
                var hlp = new int[_depth + 8];
                Array.Copy(_nspCounts, 0, hlp, 0, _depth + 1);
                _nspCounts = hlp;
            }

            _nspCounts[_depth + 1] = _nspCounts[_depth];
            _writer.Write(close ? " />" : ">");
        }

        private void WriteEscaped(string str, int quot)
        {
            foreach (var c in str)
            {
                switch (c)
                {
                    case '\n':
                    case '\r':
                    case '\t':
                        if (quot == -1)
                            _writer.Write(c);
                        else
                            _writer.Write("&#" + ((int) c) + ';');
                        break;
                    case '&':
                        _writer.Write("&amp;");
                        break;
                    case '>':
                        _writer.Write("&gt;");
                        break;
                    case '<':
                        _writer.Write("&lt;");
                        break;
                    case '"':
                    case '\'':
                        if (c == quot)
                        {
                            _writer.Write(c == '"' ? "&quot;" : "&apos;");
                        }
                        else
                        {
                            if (c >= ' ' && c != '@' && (c < 127 || _unicode))
                                _writer.Write(c);
                            else
                                _writer.Write("&#" + ((int) c) + ";");
                        }
                        break;
                    default:
                        if (c >= ' ' && c != '@' && (c < 127 || _unicode))
                            _writer.Write(c);
                        else
                            _writer.Write("&#" + ((int) c) + ";");
                        break;

                }
            }
        }

        public void SetFeature(string name, bool value)
        {
            if ("http://xmlpull.org/v1/doc/features.html#indent-output".Equals(name))
                _indent[_depth] = value;
            else
                throw new Exception("Unsupported Feature");
        }

        public bool GetFeature(string name)
        {
            return ("http://xmlpull.org/v1/doc/features.html#indent-output".Equals(name)) && _indent[_depth];
        }

        public void SetProperty(string name, object value)
        {
            throw new NotImplementedException();
        }

        public object GetProperty(string name)
        {
            throw new NotImplementedException();
        }

        public void SetOutput(Stream os, Encoding encoding)
        {
            if (os == null)
                throw new ArgumentException();

            SetOutput(encoding == null ? new StreamWriter(os) : new StreamWriter(os, encoding));
            _encoding = encoding;
            if (encoding != null && encoding.WebName.ToLower().StartsWith("utf"))
                _unicode = true;
        }

        public void SetOutput(TextWriter writer)
        {
            _writer = writer;
            _nspCounts[0] = 2;
            _nspCounts[1] = 2;
            _nspStack[0] = "";
            _nspStack[1] = "";
            _nspStack[2] = "xml";
            _nspStack[3] = "http://www.w3.org/XML/1998/namespace";
            _pending = false;
            _auto = 0;
            _depth = 0;
            _unicode = false;
        }

        public void StartDocument(Encoding encoding, bool? standalone)
        {
            _writer.Write("<?xml version='1.0' ");

            if (encoding != null)
            {
                _encoding = encoding;
                if (encoding.WebName.ToLower().StartsWith("utf"))
                    _unicode = true;
            }

            if (_encoding != null)
            {
                _writer.Write("encoding='");
                _writer.Write(_encoding.WebName);
                _writer.Write("' ");
            }

            if (standalone != null)
            {
                _writer.Write("standalone='");
                _writer.Write(standalone.Value ? "yes" : "no");
                _writer.Write("' ");
            }
            _writer.Write("?>");
        }

        public void EndDocument()
        {
            while (_depth > 0)
            {
                EndTag(_elementStack[_depth*3 - 3], _elementStack[_depth*3 - 1]);
            }
            Flush();
        }

        public void SetPrefix(string prefix, string _namespace)
        {
            Check(false);

            if (prefix == null)
                prefix = "";
            if (_namespace == null)
                _namespace = "";

            var defined = GetPrefix(_namespace, true, false);

            if (prefix.Equals(defined))
                return;

            var pos = (_nspCounts[_depth + 1]++) << 1;

            if (_nspStack.Length < pos + 1)
            {
                var hlp = new string[_nspStack.Length + 16];
                Array.Copy(_nspStack, 0, hlp, 0, pos);
                _nspStack = hlp;
            }

            _nspStack[pos++] = prefix;
            _nspStack[pos] = _namespace;
        }

        public string GetPrefix(string _namespace, bool create)
        {
            return GetPrefix(_namespace, false, create);
        }

        private string GetPrefix(string _namespace, bool includeDefault, bool create)
        {

            for (var i = _nspCounts[_depth + 1]*2 - 2;
                i >= 0;
                i -= 2)
            {
                if (_nspStack[i + 1].Equals(_namespace) && (includeDefault || !_nspStack[i].Equals("")))
                {
                    var cand = _nspStack[i];
                    for (var j = i + 2; j < _nspCounts[_depth + 1]*2; j++)
                    {
                        if (_nspStack[j].Equals(cand))
                        {
                            cand = null;
                            break;
                        }
                    }
                    if (cand != null)
                        return cand;
                }
            }

            if (!create)
                return null;

            string prefix;

            if ("".Equals(_namespace))
                prefix = "";
            else
            {
                do
                {
                    prefix = "n" + (_auto++);
                    for (var i = _nspCounts[_depth + 1]*2 - 2; i >= 0; i -= 2)
                    {
                        if (!prefix.Equals(_nspStack[i]))
                            continue;

                        prefix = null;
                        break;
                    }
                } while (prefix == null);
            }

            var p = _pending;
            _pending = false;
            SetPrefix(prefix, _namespace);
            _pending = p;
            return prefix;
        }

        public int GetDepth()
        {
            return _pending ? _depth + 1 : _depth;
        }

        public string GetNamespace()
        {
            return GetDepth() == 0 ? null : _elementStack[GetDepth()*3 - 3];
        }

        public string GetName()
        {
            return GetDepth() == 0 ? null : _elementStack[GetDepth()*3 - 1];
        }

        public IXmlSerializer StartTag(string namespace_, string name)
        {
            Check(false);

            if (_indent[_depth])
            {
                _writer.Write("\r\n");
                for (var i = 0; i < _depth; i++)
                    _writer.Write("  ");
            }

            var esp = _depth*3;

            if (_elementStack.Length < esp + 3)
            {
                var hlp = new string[_elementStack.Length + 12];
                Array.Copy(_elementStack, 0, hlp, 0, esp);
                _elementStack = hlp;
            }

            var prefix = 
                namespace_ == null ? "" : GetPrefix(namespace_,true, true);

            if ("".Equals(namespace_))
            {
                for (var i = _nspCounts[_depth]; i < _nspCounts[_depth + 1]; i++)
                {
                    if ("".Equals(_nspStack[i*2]) && !"".Equals(_nspStack[i*2 + 1]))
                        throw new InvalidOperationException("Cannot set default namespace for elements in no namespace");
                }
            }

            _elementStack[esp++] = namespace_;
            _elementStack[esp++] = prefix;
            _elementStack[esp] = name;

            _writer.Write('<');
            if (!"".Equals(prefix))
            {
                _writer.Write(prefix);
                _writer.Write(':');
            }

            _writer.Write(name);

            _pending = true;

            return this;
        }

        public IXmlSerializer Attribute(string _namespace, string name, string value)
        {
            if (!_pending)
                throw new InvalidOperationException("illegal position for Attribute");

            if (_namespace == null)
                _namespace = "";

            var prefix = 
                "".Equals(_namespace) ? "" : GetPrefix(_namespace, false, true);

            _writer.Write(' ');
            if (!"".Equals(prefix))
            {
                _writer.Write(prefix);
                _writer.Write(':');
            }
            _writer.Write(name);
            _writer.Write('=');
            var q = value.IndexOf('"') == -1 ? '"' : '\'';
            _writer.Write(q);
            WriteEscaped(value, q);
            _writer.Write(q);

            return this;
        }

        public IXmlSerializer EndTag(string _namespace, string name)
        {
            if (!_pending)
                _depth--;

            if (_namespace == null && _elementStack[_depth*3] != null
                || _namespace != null && !_namespace.Equals(_elementStack[_depth*3])
                || !_elementStack[_depth*3 + 2].Equals(name))
                throw new ArgumentException($"</{ _namespace }{name}> does not match start");

            if (_pending)
            {
                Check(true);
                _depth--;
            }
            else
            {
                if (_indent[_depth + 1])
                {
                    _writer.Write("\r\n");
                    for (var i = 0; i < _depth; i++)
                        _writer.Write("  ");
                }

                _writer.Write("</");
                var prefix = _elementStack[_depth*3 + 1];
                if (!"".Equals(prefix))
                {
                    _writer.Write(prefix);
                    _writer.Write(':');
                }
                _writer.Write(name);
                _writer.Write('>');
            }

            _nspCounts[_depth + 1] = _nspCounts[_depth];
            return this;
        }

        public IXmlSerializer StartTag(string prefix, string _namespace, string name)
        {
            throw new NotImplementedException();
        }

        public IXmlSerializer Attribute(string prefix, string _namespace, string name, string value)
        {
            throw new NotImplementedException();
        }

        public IXmlSerializer EndTag(string prefix, string _namespace, string name)
        {
            throw new NotImplementedException();
        }

        public IXmlSerializer Text(string text)
        {
            Check(false);
            _indent[_depth] = false;
            WriteEscaped(text, -1);
            return this;
        }

        public IXmlSerializer Text(char[] buf, int start, int len)
        {
            return Text(new string(buf, start, len));
        }

        public void CdSect(string data)
        {
            Check(false);
            _writer.Write("<![CDATA[");
            _writer.Write(data);
            _writer.Write("]]>");
        }

        public void EntityRef(string name)
        {
            Check(false);
            _writer.Write('&');
            _writer.Write(name);
            _writer.Write(';');
        }

        public void ProcessingInstruction(string pi)
        {
            Check(false);
            _writer.Write("<?");
            _writer.Write(pi);
            _writer.Write("?>");
        }

        public void Comment(string text)
        {
            Check(false);
            _writer.Write("<!--");
            _writer.Write(text);
            _writer.Write("-->");
        }

        public void DoCdecl(string dd)
        {
            _writer.Write("<!DOCTYPE");
            _writer.Write(dd);
            _writer.Write(">");
        }

        public void IgnorableWhitespace(string s)
        {
            Text(s);
        }

        public void Flush()
        {
            Check(false);
            _writer.Flush();
        }
    }
}
