﻿using System.IO;

namespace kSOAPXamarin.Xml.XmlPull
{
    public abstract class XmlPullParser
    {
        public const string NoNamespace = "";
        public const int StartDocument = 0;
        public const int EndDocument = 1;
        public const int StartTag = 2;
        public const int EndTag = 3;
        public const int Text = 4;
        public const int CdSect = 5;
        public const int EntityRef = 6;
        public const int IgnorableWhitespace = 7;
        public const int ProcessingInstruction = 8;
        public const int Comment = 9;
        public const int DocDecl = 10;
        public static readonly string[] Types =
        {
            "START_DOCUMENT",
            "END_DOCUMENT",
            "START_TAG",
            "END_TAG",
            "TEXT",
            "CDSECT",
            "ENTITY_REF",
            "IGNORABLE_WHITESPACE",
            "PROCESSING_INSTRUCTION",
            "COMMENT",
            "DOCDECL"
        };

        public const string FeatureProcessNamespaces = "http://xmlpull.org/v1/doc/features.html#process-namespaces";
        public const string FeatureReportNamespaceAttributes = "http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes";
        public const string FeatureProcessDocDecl = "http://xmlpull.org/v1/doc/features.html#process-DoCdecl";
        public const string FeatureValidation = "http://xmlpull.org/v1/doc/features.html#validation";

        /// <summary>
        /// Use this call to change the general behavior of the parser,
        /// such as namespace processing or doctype declaration handling.
        /// This method must be called before the first call to Next or
        /// NextToken. Otherwise, an exception is thrown.
        /// Example: call SetFeature(FEATURE_PROCESS_NAMESPACES, true) in order
        /// to switch on namespace processing. The initial settings correspond
        /// to the properties requested from the XML Pull Parser factory.
        /// If none were requested, all features are deactivated by default.
        /// </summary>
        public abstract void SetFeature(string name, bool state);

        /// <summary>
        /// Returns the current value of the given feature.
        /// Please note: unknown features are returned as false.
        /// </summary>
        public abstract bool GetFeature(string name);

        /// <summary>
        /// Set the value of a property.
        /// </summary>
        public abstract void SetProperty(string name, object value);

        /// <summary>
        /// Look up the value of a property.
        /// </summary>
        public abstract object GetProperty(string name);

        /// <summary>
        /// Set the input source for parser to the given reader and
        /// resets the parser. The event type is set to the initial value
        /// START_DOCUMENT.
        /// Setting the reader to null will just stop parsing and
        /// reset parser state,
        /// allowing the parser to free internal resources
        /// such as parsing buffers.
        /// </summary>
        public abstract void SetInput(TextReader input);

        /// <summary>
        /// Sets the input stream the parser is going to process.
        /// This call resets the parser state and sets the event type
        /// to the initial value START_DOCUMENT.
        /// </summary>
        public abstract void SetInput(Stream inputStream, string inputEncoding);

        /// <summary>
        /// Returns the input encoding if known, null otherwise.
        /// If SetInput(InputStream, inputEncoding) was called with an inputEncoding
        /// value other than null, this value must be returned
        /// from this method. Otherwise, if inputEncoding is null and
        /// the parser supports the encoding detection feature
        /// (http://xmlpull.org/v1/doc/features.html#detect-encoding),
        /// it must return the detected encoding.
        /// If SetInput(Reader) was called, null is returned.
        /// After first call to Next if XML declaration was present this method
        /// will return encoding declared.
        /// </summary>
        public abstract string GetInputEncoding();

        /// <summary>
        /// Set new value for entity replacement Text as defined in
        /// http://www.w3.org/TR/REC-xml#intern-replacement XML 1.0 Section 4.5
        /// Construction of Internal Entity Replacement Text&lt;/a&gt;.
        /// If FEATURE_PROCESS_DOCDECL or FEATURE_VALIDATION are set, calling this
        /// function will result in an exception -- when processing of DOCDECL is
        /// enabled, there is no need to the entity replacement Text manually.
        /// </summary>
        public abstract void DefineEntityReplacementText(string entityName, string replacementText);

        /// <summary>
        /// Returns the numbers of elements in the namespace stack for the given depth.
        /// </summary>
        public abstract int GetNamespaceCount(int depth);

        /// <summary>
        /// Returns the namespace prefixes for the given position.
        /// in the namespace stack.
        /// </summary>
        public abstract string GetNamespacePrefix(int pos);

        /// <summary>
        /// Returns the namespace URI for the given position in the namespace stack.
        /// </summary>
        public abstract string GetNamespaceUri(int pos);

        /// <summary>
        /// Returns the URI corresponding to the given prefix,
        /// depending on current state of the parser.
        /// </summary>
        public abstract string GetNamespace(string prefix);

        /// <summary>
        /// Returns the current depth of the element.
        /// </summary>
        public abstract int GetDepth();

        /// <summary>
        /// Returns a short Text describing the current parser state, including the position,
        /// a description of the current event and the data source if known.
        /// This method is especially useful to provide meaningful
        /// error messages and for debugging purposes.
        /// </summary>
        public abstract string GetPositionDescription();

        /// <summary>
        /// Returns the current line number, starting from 1.
        /// When the parser does not know the current line number
        /// or can not determine it,  -1 is returned (e.g. for WBXML).
        /// </summary>
        public abstract int GetLineNumber();

        /// <summary>
        /// Returns the current column number, starting from 0.
        /// When the parser does not know the current column number
        /// or can not determine it,  -1 is returned (e.g. for WBXML).
        /// </summary>
        public abstract int GetColumnNumber();

        /// <summary>
        /// Checks whether the current TEXT event contains only whitespace characters.
        /// </summary>
        public abstract bool IsWhitespace();

        /// <summary>
        /// Returns the Text content of the current event as String.
        /// The value returned depends on current event type,
        /// for example for TEXT event it is element content
        /// (this is typical case when Next() is used).
        /// </summary>
        public abstract string GetText();

        /// <summary>
        /// Returns the buffer that contains the Text of the current event,
        /// as well as the start offset and length relevant for the current
        /// event. See GetText(), Next() and NextToken() for description of possible returned values.
        /// </summary>
        public abstract char[] GetTextCharacters(int[] holderForStartAndLength);

        /// <summary>
        /// Returns the namespace URI of the current element.
        /// The default namespace is represented as empty string.
        /// </summary>
        public abstract string GetNamespace();

        /// <summary>
        /// For START_TAG or END_TAG events, the (local) name of the current
        /// element is returned when namespaces are enabled. When namespace
        /// processing is disabled, the raw name is returned.
        /// For ENTITY_REF events, the entity name is returned.
        /// If the current event is not START_TAG, END_TAG, or ENTITY_REF,
        /// null is returned.
        /// </summary>
        public abstract string GetName();

        /// <summary>
        /// Returns the prefix of the current element.
        /// If the element is in the default namespace (has no prefix),
        /// null is returned.
        /// </summary>
        public abstract string GetPrefix();

        /// <summary>
        /// Returns true if the current event is START_TAG and the tag is degenerated.
        /// </summary>
        public abstract bool IsEmptyElementTag();

        /// <summary>
        /// Returns the number of attributes of the current start tag, or
        /// -1 if the current event type is not START_TAG.
        /// </summary>
        public abstract int GetAttributeCount();

        /// <summary>
        /// Returns the namespace URI of the Attribute with the given index (starts from 0).
        /// </summary>
        public abstract string GetAttributeNamespace(int index);

        /// <summary>
        /// Returns the local name of the specified Attribute
        /// if namespaces are enabled or just Attribute name if namespaces are disabled.
        /// Throws an IndexOutOfBoundsException if the index is out of range
        /// or current event type is not START_TAG.
        /// </summary>
        public abstract string GetAttributeName(int index);

        /// <summary>
        /// Returns the prefix of the specified Attribute
        /// Returns null if the element has no prefix.
        /// If namespaces are disabled it will always return null.
        /// Throws an IndexOutOfBoundsException if the index is out of range
        /// or current event type is not START_TAG.
        /// </summary>
        public abstract string GetAttributePrefix(int index);

        /// <summary>
        /// Returns the type of the specified Attribute
        /// If parser is non-validating it MUST return CDATA.
        /// </summary>
        public abstract string GetAttributeType(int index);

        /// <summary>
        /// Returns if the specified Attribute was not in input was declared in XML.
        /// If parser is non-validating it MUST always return false.
        /// </summary>
        public abstract bool IsAttributeDefault(int index);

        /// <summary>
        /// Returns the given attributes value.
        /// </summary>
        public abstract string GetAttributeValue(int index);

        /// <summary>
        /// Returns the attributes value identified by namespace URI and namespace localName.
        /// If namespaces are disabled namespace must be null.
        /// </summary>
        public abstract string GetAttributeValue(string _namespace, string name);

        /// <summary>
        /// Returns the type of the current event (START_TAG, END_TAG, TEXT, etc.)
        /// </summary>
        /// <returns></returns>
        public abstract int GetEventType();

        public abstract int Next();

        /// <summary>
        /// This method works similarly to Next() but will expose
        /// additional event types (COMMENT, CDSECT, DOCDECL, ENTITY_REF, PROCESSING_INSTRUCTION, or
        /// IGNORABLE_WHITESPACE) if they are available in input.
        /// </summary>
        public abstract int NextToken();

        /// <summary>
        /// Test if the current event is of the given type and if the
        /// namespace and name do match. null will match any namespace
        /// and any name. If the test is not passed, an exception is
        /// thrown. The exception Text indicates the parser position,
        /// the expected event and the current event that is not meeting the requirement.
        /// </summary>
        public abstract void Require(int type, string _namespace, string name);

        /// <summary>
        /// If current event is START_TAG then if Next element is TEXT then element content is returned
        /// or if Next event is END_TAG then empty string is returned, otherwise exception is thrown.
        /// After calling this function successfully parser will be positioned on END_TAG.
        /// </summary>
        public abstract string NextText();

        /// <summary>
        /// Call Next() and return event if it is START_TAG or END_TAG
        /// otherwise throw an exception.
        /// </summary>
        public abstract int NextTag();

        /// <summary>
        /// Skip sub tree on which the parser is currently positioned on.
        /// </summary>
        public abstract void SkipSubTree();

    }
}
