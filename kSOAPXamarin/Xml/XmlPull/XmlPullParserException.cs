﻿using System;

namespace kSOAPXamarin.Xml.XmlPull
{
    public class XmlPullParserException : Exception
    {
        protected Exception Detail;
        protected int Row = -1;
        protected int Column = -1;

        public XmlPullParserException(string s) : base(s)
        {
        }

        public XmlPullParserException(string msg, XmlPullParser parser, Exception chain)
            : base((msg == null ? "" : msg + " ") + (parser == null ? "" : "(position:" + parser.GetPositionDescription() + ") ") + (chain == null ? "" : "caused by: " + chain))
        {
            if (parser != null)
            {
                Row = parser.GetLineNumber();
                Column = parser.GetColumnNumber();
            }
            Detail = chain;
        }

        public Exception GetDetail => Detail;

        public int GetLineNumber => Row;

        public int GetColumnNumber => Column;

        public override string StackTrace
        {
            get
            {
                if (Detail == null)
                    return base.StackTrace;

                return Message + "; nested exception is:" + Detail.StackTrace;
            }
        }
    }
}
