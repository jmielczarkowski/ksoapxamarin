﻿using System.IO;
using System.Text;

namespace kSOAPXamarin.Xml.XmlPull
{
    public interface IXmlSerializer
    {
        /// <summary>
        /// Set feature identified by name (recommended to be URI for uniqueness).
        /// </summary>
        void SetFeature(string name, bool state);

        /// <summary>
        /// Return the current value of the feature with given name.
        /// </summary>
        bool GetFeature(string name);

        /// <summary>
        /// Set the value of a property.
        /// </summary>
        void SetProperty(string name, object value);
        
        /// <summary>
        /// Look up the value of a property.
        /// </summary>
        object GetProperty(string name);

        /// <summary>
        /// Set to use binary output stream with given encoding.
        /// </summary>
        void SetOutput(Stream os, Encoding encoding);

        /// <summary>
        /// Set the output to the given writer.
        /// </summary>
        void SetOutput(TextWriter writer);

        /// <summary>
        /// Write xml declaration with encoding (if encoding not null)
        /// and standalone flag (if standalone not null)
        /// This method can only be called just after SetOutput.
        /// </summary>
        void StartDocument(Encoding encoding, bool? standalone);

        /// <summary>
        /// Finish writing. All unclosed start tags will be closed and output
        /// will be flushed. After calling this method no more output can be
        /// serialized until Next call to SetOutput().
        /// </summary>
        void EndDocument();

        /// <summary>
        /// Binds the given prefix to the given namespace.
        /// This call is valid for the Next element including child elements.
        /// The prefix and namespace MUST be always declared even if prefix
        /// is not used in element (StartTag() or Attribute()) - for XML 1.0
        /// it must result in declaring &lt;code&gt;xmlns:prefix='namespace'&lt;/code&gt;
        /// (or &lt;code&gt;xmlns:prefix="namespace"&lt;/code&gt; depending what character is used
        /// to quote Attribute value).
        /// </summary>
        void SetPrefix(string prefix, string _namespace);

        /// <summary>
        /// Return namespace that corresponds to given prefix
        /// If there is no prefix bound to this namespace return null
        /// but if generatePrefix is false then return generated prefix.
        /// </summary>
        string GetPrefix(string _namespace, bool generatePrefix);

        /// <summary>
        /// Returns the current depth of the element.
        /// </summary>
        int GetDepth();

        /// <summary>
        /// Returns the namespace URI of the current element as set by StartTag().
        /// </summary>
        string GetNamespace();

        /// <summary>
        /// Returns the name of the current element as set by StartTag().
        /// </summary>
        string GetName();

        /// <summary>
        /// Writes a start tag with the given namespace and name.
        /// If there is no prefix defined for the given namespace,
        /// a prefix will be defined automatically.
        /// </summary>
        IXmlSerializer StartTag(string namespace_, string name);

        /// <summary>
        /// Write an Attribute. Calls to Attribute() MUST follow a call to
        /// StartTag() immediately. If there is no prefix defined for the
        /// given namespace, a prefix will be defined automatically.
        /// If namespace is null or empty string
        /// no namespace prefix is printed but just name.
        /// </summary>
        IXmlSerializer Attribute(string _namespace, string name, string value);

        /// <summary>
        /// Write end tag. Repetition of namespace and name is just for avoiding errors.
        /// </summary>
        IXmlSerializer EndTag(string _namespace, string name);

        /// <summary>
        /// Writes a start tag with the given namespace and name.
        /// </summary>
        IXmlSerializer StartTag(string prefix, string _namespace, string name);

        /// <summary>
        /// Write an Attribute. Calls to Attribute() MUST follow a call to StartTag() immediately.
        /// </summary>
        IXmlSerializer Attribute(string prefix, string _namespace, string name, string value);

        /// <summary>
        /// Write end tag. Repetition of namespace, prefix, and name is just for avoiding errors.
        /// </summary>
        IXmlSerializer EndTag(string prefix, string _namespace, string name);

        /// <summary>
        /// Writes Text, where special XML chars are escaped automatically.
        /// </summary>
        IXmlSerializer Text(string text);

        /// <summary>
        /// Writes Text, where special XML chars are escaped automatically.
        /// </summary>
        IXmlSerializer Text(char[] buf, int start, int len);

        void CdSect(string text);

        void EntityRef(string text);

        void ProcessingInstruction(string text);

        void Comment(string text);

        void DoCdecl(string text);

        void IgnorableWhitespace(string text);

        /// <summary>
        /// Write all pending output to the stream.
        /// </summary>
        void Flush();

    }
}
