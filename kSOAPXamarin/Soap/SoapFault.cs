﻿using System;
using System.IO;
using kSOAPXamarin.Xml.Dom;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap
{
    public class SoapFault : IOException
    {
        /// <summary>
        /// The SOAP fault code.
        /// </summary>
        public string FaultCode;

        /// <summary>
        /// The SOAP fault code.
        /// </summary>
        public string FaultString;

        /// <summary>
        /// The SOAP fault actor.
        /// </summary>
        public string FaultActor;

        /// <summary>
        /// A KDom Node holding the details of the fault.
        /// </summary>
        public Node Detail;

        /// <summary>
        /// An integer that holds current soap version.
        /// </summary>
        public int Version;

        public SoapFault()
        {
            Version = SoapEnvelope.Ver11;
        }

        public SoapFault(int version)
        {
           Version = version;
        }

        /// <summary>
        /// Fills the fault details from the given XML stream.
        /// </summary>
        public virtual void Parse(XmlPullParser parser)
        {
            parser.Require(XmlPullParser.StartTag, SoapEnvelope.Env, "Fault");
            while (parser.NextTag() == XmlPullParser.StartTag)
            {
                var name = parser.GetName();
                if (name.Equals("Detail"))
                {
                    Detail = new Node();
                    Detail.Parse(parser);

                    if (parser.GetNamespace().Equals(SoapEnvelope.Env) && parser.GetName().Equals("Fault"))
                    {
                        break;
                    }
                    continue;
                }
                if (name.Equals("faultcode"))
                {
                    FaultCode = parser.NextText();
                }
                else if (name.Equals("faultstring"))
                {
                    FaultString = parser.NextText();
                }
                else if (name.Equals("faultactor"))
                {
                    FaultActor = parser.NextText();
                }
                else
                    throw new Exception($"Unexpected tag: {name}.");

                parser.Require(XmlPullParser.EndTag, null, name);
            }
            parser.Require(XmlPullParser.EndTag, SoapEnvelope.Env, "Fault");
            parser.NextTag();
        }

        /// <summary>
        /// Writes the fault to the given XML stream.
        /// </summary>
        public virtual void Write(IXmlSerializer xw)
        {
            xw.StartTag(SoapEnvelope.Env, "Fault");
            xw.StartTag(null, "faultcode");
            xw.Text("" + FaultCode);
            xw.EndTag(null, "faultcode");
            xw.StartTag(null, "faultstring");
            xw.Text("" + FaultString);
            xw.EndTag(null, "faultstring");
            xw.StartTag(null, "Detail");
            Detail?.Write(xw);
            xw.EndTag(null, "Detail");
            xw.EndTag(SoapEnvelope.Env, "Fault");
        }

        public override string Message => FaultString;

        /// <summary>
        /// Returns a simple string representation of the fault.
        /// </summary>
        public override string ToString()
        {
            return $"FaultCode: {FaultCode}, faultString: {FaultString}, FaultActor: {FaultActor}, Detail {Detail}";
        }
    }

    public class SoapFault12 : SoapFault
    {
        /** Top-level nodes */
        public Node Code;
        public Node Reason;
        public Node Node;
        public Node Role;
        public new Node Detail;

        public SoapFault12()
        {
            Version = SoapEnvelope.Ver12;
        }

        public SoapFault12(int version)
        {
            Version = version;
        }

        /// <summary>
        /// Fills the fault details from the given XML stream.
        /// </summary>
        public override void Parse(XmlPullParser parser)
        {
            ParseSelf(parser); 
            FaultCode = Code.GetElement(SoapEnvelope.Env2003, "Value").GetText(0);
            FaultString = Reason.GetElement(SoapEnvelope.Env2003, "Text").GetText(0);
            base.Detail = Detail;
            FaultActor = null;
        }

        private void ParseSelf(XmlPullParser parser)
        {
            parser.Require(XmlPullParser.StartTag, SoapEnvelope.Env2003, "Fault");

            while (parser.NextTag() == XmlPullParser.StartTag)
            {
                var name = parser.GetName();
                var namespace_ = parser.GetNamespace();
                parser.NextTag();
                if (name.ToLower().Equals("Code".ToLower()))
                {
                    Code = new Node();
                    Code.Parse(parser);
                }
                else if (name.ToLower().Equals("Reason".ToLower()))
                {
                    Reason = new Node();
                    Reason.Parse(parser);
                }
                else if (name.ToLower().Equals("Node".ToLower()))
                {
                    Node = new Node();
                    Node.Parse(parser);
                }
                else if (name.ToLower().Equals("Role".ToLower()))
                {
                    Role = new Node();
                    Role.Parse(parser);
                }
                else if (name.ToLower().Equals("Detail".ToLower()))
                {
                    Detail = new Node();
                    Detail.Parse(parser);
                }
                else
                    throw new Exception($"Unexpected tag: {name}");

                parser.Require(XmlPullParser.EndTag, namespace_, name);
            }
            parser.Require(XmlPullParser.EndTag, SoapEnvelope.Env2003, "Fault");
            parser.NextTag();

        }

        /// <summary>
        /// Writes the fault to the given XML stream.
        /// </summary>
        public override void Write(IXmlSerializer xw)
        {
            xw.StartTag(SoapEnvelope.Env2003, "Fault");
            xw.StartTag(SoapEnvelope.Env2003, "Code");
            Code.Write(xw);
            xw.EndTag(SoapEnvelope.Env2003, "Code");
            xw.StartTag(SoapEnvelope.Env2003, "Reason");
            Reason.Write(xw);
            xw.EndTag(SoapEnvelope.Env2003, "Reason");

            if (Node != null)
            {
                xw.StartTag(SoapEnvelope.Env2003, "Node");
                Node.Write(xw);
                xw.EndTag(SoapEnvelope.Env2003, "Node");
            }
            if (Role != null)
            {
                xw.StartTag(SoapEnvelope.Env2003, "Role");
                Role.Write(xw);
                xw.EndTag(SoapEnvelope.Env2003, "Role");
            }

            if (Detail != null)
            {
                xw.StartTag(SoapEnvelope.Env2003, "Detail");
                Detail.Write(xw);
                xw.EndTag(SoapEnvelope.Env2003, "Detail");
            }
            xw.EndTag(SoapEnvelope.Env2003, "Fault");
        }

        public override string Message => Reason.GetElement(SoapEnvelope.Env2003, "Text").GetText(0);

        /// <summary>
        /// Returns a simple string representation of the fault.
        /// </summary>
        public override string ToString()
        {
            var reason = Reason.GetElement(SoapEnvelope.Env2003, "Text").GetText(0);
            var code = Code.GetElement(SoapEnvelope.Env2003, "Value").GetText(0);
            return $"Code: {code}, Reason: {reason}";
        }
    }
}
