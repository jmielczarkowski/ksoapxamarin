﻿using System;
using System.Collections.Generic;
using System.IO;

namespace kSOAPXamarin.Soap.Transport
{
    public class HttpResponseException : IOException
    {
        private readonly int _statusCode;
        private readonly List<HeaderProperty> _responseHeaders;

        public HttpResponseException(int statusCode)
        {
            _statusCode = statusCode;
        }

        public HttpResponseException(string detailMessage, int statusCode)
            : base(detailMessage)
        {
            _statusCode = statusCode;
        }

        public HttpResponseException(string detailMessage, int statusCode, List<HeaderProperty> responseHeaders)
            : base(detailMessage)
        {
            _statusCode = statusCode;
            _responseHeaders = responseHeaders;
        }

        public HttpResponseException(string message, Exception cause, int statusCode) : base(message, cause)
        {
            _statusCode = statusCode;
        }

        public HttpResponseException(Exception cause, int statusCode) : base(null, cause)
        {
            _statusCode = statusCode;
        }

        /// <summary>
        /// Returns the unexpected Http response code.
        /// </summary>
        public int GetStatusCode()
        {
            return _statusCode;
        }

        /// <summary>
        /// Returns all http headers from this response
        /// </summary>
        public List<HeaderProperty> GetResponseHeaders()
        {
            return _responseHeaders;
        }
    }
}
