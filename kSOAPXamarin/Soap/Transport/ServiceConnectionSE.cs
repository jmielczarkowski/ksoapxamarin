﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
// ReSharper disable InconsistentNaming

namespace kSOAPXamarin.Soap.Transport
{
    public class ServiceConnectionSE : ServiceConnection
    {
        private readonly HttpClient _client;
        private readonly HttpRequestMessage _request;
        private Task<HttpResponseMessage> _responseTask;
        private HttpResponseMessage _response;
        private Stream _inStream;
        private readonly int _timeout;
        // ReSharper disable once NotAccessedField.Local
        private int _fixedLength = -1;
        private MediaTypeHeaderValue _contentType;

        private void CheckRespond()
        {
            if (_response != null)
                return;

            if (!_responseTask.IsCompleted)
            {
                if (!_responseTask.Wait(_timeout))
                {
                    throw new TimeoutException();
                }
            }
            _response = _responseTask.Result;
        }

        /// <summary>
        /// Constructor taking the url to the endpoint for this soap communication
        /// url the url to open the connection to.
        /// </summary>
        public ServiceConnectionSE(string url) : this(null, url, DefaultTimeout) { }

        public ServiceConnectionSE(IWebProxy proxy, string url) : this(proxy, url, DefaultTimeout) { }

        /// <summary>
        /// Constructor taking the url to the endpoint for this soap communication.
        /// </summary>
        public ServiceConnectionSE(string url, int timeout) : this(null, url, timeout) { }

        public ServiceConnectionSE(IWebProxy proxy, string url, int timeout)
        {
            _timeout = timeout;
            var handler = new HttpClientHandler {Proxy = proxy};
            _client = new HttpClient(handler);
            _request = new HttpRequestMessage {RequestUri = new Uri(url)};
        }

        public override void Connect()
        {
            _responseTask = _client.SendAsync(_request);
        }

        public override void Disconnect()
        {
            _client.Dispose();
        }

        public override List<HeaderProperty> GetResponseProperties()
        {
            CheckRespond();

            var prop1 = (from httpResponseHeader in _response.Headers from s in httpResponseHeader.Value select new HeaderProperty(httpResponseHeader.Key, s)).ToList();
            var prop2 = (from httpContentHeader in _response.Content.Headers from s in httpContentHeader.Value select new HeaderProperty(httpContentHeader.Key, s)).ToList();

            prop1.AddRange(prop2);

            return prop1;
        }

        public override int GetResponseCode()
        {
            CheckRespond();

            return Convert.ToInt32(_response.StatusCode);
        }

        public override void SetRequestProperty(string propertyName, string value)
        {
            var low = propertyName.ToLower();
            if ("content-type".Equals(low))
            {
                _contentType = MediaTypeHeaderValue.Parse(value);
            }
            else
            {
                _request.Headers.TryAddWithoutValidation(propertyName, value);
            }
        }

        public override void SetRequestMethod(string requestMethodType)
        {
            _request.Method = new HttpMethod(requestMethodType);
        }

        /// <summary>
        /// If the length of a HTTP request body is known ahead, sets fixed length 
        /// to enable streaming without buffering. Sets after connection will cause an exception.
        /// </summary>
        public override void SetFixedLengthStreamingMode(int contentLength)
        {
            _fixedLength = contentLength;
        }

        public override void SetChunkedStreamingMode()
        {
        }

        public override void SendData(byte[] data)
        {
            var ctx = new StreamContent(new MemoryStream((byte[])data.Clone()));
            ctx.Headers.ContentType = _contentType;
            _request.Content = ctx;
            Connect();
        }

        public override Stream OpenInputStream()
        {
            if (_inStream != null)
            {
                return _inStream;
            }

            CheckRespond();
            var stream = _response.Content.ReadAsStreamAsync();
            if (!stream.Wait(_timeout))
                throw new TimeoutException();

            _inStream = stream.Result;
            return _inStream;

        }

        public override Stream GetErrorStream()
        {
            throw new NotImplementedException();
        }

        public override string GetHost()
        {
            return _request.RequestUri.Host;
        }

        public override int GetPort()
        {
            return _request.RequestUri.Port;
        }

        public override string GetPath()
        {
            return _request.RequestUri.AbsolutePath;
        }
    }
}
