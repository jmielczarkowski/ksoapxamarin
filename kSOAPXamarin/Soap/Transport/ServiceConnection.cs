﻿using System.Collections.Generic;
using System.IO;

namespace kSOAPXamarin.Soap.Transport
{
    /// <summary>
    /// Interface to allow the abstraction of the raw transport information.
    /// </summary>
    public abstract class ServiceConnection
    {
        public const int DefaultTimeout = 20000; // by default we setup it to 20 seconds.
        public const int DefaultBufferSize = 256 * 1024;

        /// <summary>
        /// Make an outgoing connection.
        /// </summary>
        public abstract void Connect();

        /// <summary>
        /// Disconnect from the outgoing connection.
        /// </summary>
        public abstract void Disconnect();

        /// <summary>
        /// Returns to the caller all of the headers that were returned with the
        /// response to the SOAP request. Primarily this gives the caller an 
        /// opportunity to save the cookies for later use.
        /// </summary>
        public abstract List<HeaderProperty> GetResponseProperties();

        /// <summary>
        /// Returns the numerical HTTP status to the caller.
        /// </summary>
        public abstract int GetResponseCode();

        /// <summary>
        /// Set properties on the outgoing connection.
        /// </summary>
        public abstract void SetRequestProperty(string propertyName, string value);

        /// <summary>
        /// Sets how to make the requests. For HTTP this is typically POST or GET.
        /// </summary>
        public abstract void SetRequestMethod(string requestMethodType);

        /// <summary>
        /// If the length of a HTTP request body is known ahead, sets fixed length 
        /// to enable streaming without buffering. Sets after connection will cause an exception.
        /// </summary>
        public abstract void SetFixedLengthStreamingMode(int contentLength);

        public abstract void SetChunkedStreamingMode();

        /// <summary>
        /// Open and return the outputStream to the endpoint.
        /// </summary>
        public abstract Stream OpenInputStream();

        /// <summary>
        /// Gets the error stream for the call.
        /// </summary>
        public abstract Stream GetErrorStream();

        public abstract void SendData(byte[] data);

        /// <summary>
        /// Return the name of the host that is specified as the web service target.
        /// </summary>
        public abstract string GetHost();

        /// <summary>
        /// Return the port number of the host that is specified as the web service target.
        /// </summary>
        public abstract int GetPort();

        /// <summary>
        /// Return the path to the web service target.
        /// </summary>
        public abstract string GetPath();
    }
}
