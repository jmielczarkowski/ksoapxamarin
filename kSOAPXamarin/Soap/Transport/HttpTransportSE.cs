﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

// ReSharper disable RedundantAssignment
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedParameter.Local
namespace kSOAPXamarin.Soap.Transport
{
    /// <summary>
    /// Http transport SE implementation.
    /// </summary>
    public class HttpTransportSE : Transport
    {
        /// <summary>
        /// Creates instance of HttpTransportSE with set url.
        /// </summary>
        public HttpTransportSE(string url) : base(null, url) { }

        /// <summary>
        /// Creates instance of HttpTransportSE with set url and defines a
        /// proxy server to use to access it.
        /// </summary>
        public HttpTransportSE(IWebProxy proxy, string url): base(proxy, url) { }

        /// <summary>
        ///  Creates instance of HttpTransportSE with set url.
        /// </summary>
        public HttpTransportSE(string url, int timeout) : base(url, timeout) { }

        public HttpTransportSE(IWebProxy proxy, string url, int timeout) : base(proxy, url, timeout) { }

        /// <summary>
        /// Creates instance of HttpTransportSE with set url.
        /// </summary>
        public HttpTransportSE(string url, int timeout, int contentLength) : base(url, timeout) { }

        public HttpTransportSE(IWebProxy proxy, string url, int timeout, int contentLength) : base(proxy, url, timeout) { }

        /// <summary>
        /// Set the desired soapAction header field.
        /// </summary>
        public override void Call(string soapAction, SoapEnvelope envelope)
        {
            Call(soapAction, envelope, null);
        }

        public override List<HeaderProperty> Call(string soapAction, SoapEnvelope envelope, List<HeaderProperty> headers)
        {
            return Call(soapAction, envelope, headers, null);
        }

        public override List<HeaderProperty> Call(string soapAction, SoapEnvelope envelope, List<HeaderProperty> headers, FileStream outputFile)
        {
            if (soapAction == null)
                soapAction = "\"\"";

            var requestData = CreateRequestData(envelope, "UTF-8");

            RequestDump = IsDebug ? Encoding.UTF8.GetString(requestData, 0, requestData.Length) : null;
            ResponseDump = null;

            var connection = GetServiceConnection();

            connection.SetRequestProperty("User-Agent", UserAgent);

            if (envelope.Version != SoapEnvelope.Ver12)
            {
                connection.SetRequestProperty("SOAPAction", soapAction);
            }

            connection.SetRequestProperty("Content-Type",
                envelope.Version == SoapEnvelope.Ver12
                    ? ContentTypeSoapXmlCharsetUtf8
                    : ContentTypeXmlCharsetUtf8);

            connection.SetRequestProperty("Accept-Encoding", "gzip");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    connection.SetRequestProperty(header.GetKey(), header.GetValue());
                }
            }

            connection.SetRequestMethod("POST");
            SendData(requestData, connection, envelope);
            requestData = null;
            Stream input = null;
            List<HeaderProperty> retHeaders = null;
            var contentLength = 8192;
            var gZippedContent = false;
            var xmlContent = false;
            var status = connection.GetResponseCode();

            try
            {
                retHeaders = connection.GetResponseProperties();

                foreach (var headerProperty in retHeaders)
                {
                    if (null == headerProperty.GetKey())
                        continue;

                    if (string.Equals(headerProperty.GetKey(), "content-length", StringComparison.OrdinalIgnoreCase))
                    {
                        if (headerProperty.GetValue() != null)
                        {
                            try
                            {
                                contentLength = int.Parse(headerProperty.GetValue());
                            }
                            catch (Exception)
                            {
                                contentLength = 8192;
                            }
                        }
                    }

                    if (string.Equals(headerProperty.GetKey(), "Content-Type", StringComparison.OrdinalIgnoreCase)
                        && headerProperty.GetValue().Contains("xml"))
                    {
                        xmlContent = true;
                    }

                    if (string.Equals(headerProperty.GetKey(), "Content-Encoding", StringComparison.OrdinalIgnoreCase)
                        && string.Equals(headerProperty.GetValue(), "gzip", StringComparison.OrdinalIgnoreCase))
                    {
                        gZippedContent = true;
                    }
                }

                if (status != 200)
                    throw new HttpResponseException($"HTTP request failed, HTTP status: {status}.", status, retHeaders);

                if (contentLength > 0)
                    input = gZippedContent ? GetUnZippedInputStream(connection.OpenInputStream()) 
                        : connection.OpenInputStream();
            }
            catch (Exception exception)
            {
                if (contentLength > 0)
                {
                    input = gZippedContent ? GetUnZippedInputStream(connection.OpenInputStream()) : 
                        connection.OpenInputStream();
                }

                if (exception is HttpResponseException)
                {
                    if (!xmlContent)
                    {
                        if (IsDebug && input != null)
                            ReadDebug(input, contentLength, outputFile);

                        connection.Disconnect();
                        // ReSharper disable once PossibleIntendedRethrow
                        throw exception;
                    }
                }
            }

            if (IsDebug)
                input = ReadDebug(input, contentLength, outputFile);

            ParseResponse(envelope, input, retHeaders);

            input = null;
            connection.Disconnect();
            connection = null;
            return retHeaders;
        }

        protected void SendData(byte[] requestData, ServiceConnection connection, SoapEnvelope envelope)
        {
            connection.SetRequestProperty("Content-Length", "" + requestData.Length);
            connection.SetFixedLengthStreamingMode(requestData.Length);
            connection.SendData(requestData);
        }

        protected void ParseResponse(SoapEnvelope envelope, Stream input, List<HeaderProperty> returnedHeaders)
        {
            ParseResponse(envelope, input);
        }

        private Stream ReadDebug(Stream input, int contentLength, FileStream outputFile)
        {
            var bos = new MemoryStream((contentLength > 0) ? contentLength : 256*1024);
            var buf = new byte[256];

            while (true)
            {
                var rd = input.Read(buf, 0, 256);
                if (rd == 0)
                    break;

                bos.Write(buf, 0, rd);
            }

            bos.Flush();
            // ReSharper disable once IsExpressionAlwaysTrue
            if (bos is MemoryStream)
            {
                buf = bos.ToArray();
            }
            bos = null;
            ResponseDump = Encoding.UTF8.GetString(buf, 0, buf.Length);
            input.Dispose();

            return new MemoryStream(buf);
        }

        private Stream GetUnZippedInputStream(Stream inputStream)
        {
            return inputStream as GZipStream ?? new GZipStream(inputStream, CompressionMode.Decompress);
        }

        public override ServiceConnection GetServiceConnection()
        {
            return new ServiceConnectionSE(Proxy, Url, Timeout);
        }
    }
}
