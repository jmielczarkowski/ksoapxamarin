﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using kSOAPXamarin.Xml.IO;
using kSOAPXamarin.Xml.XmlPull;

// ReSharper disable UnusedMember.Global
// ReSharper disable RedundantAssignment
namespace kSOAPXamarin.Soap.Transport
{
    public abstract class Transport
    {
        /// <summary>
        /// Added to enable web service interactions on the emulator to be debugged
        /// with Fiddler2 (Windows) but provides utility for other proxy requirements.
        /// </summary>
        protected IWebProxy Proxy;
        protected string Url;
        protected int Timeout = ServiceConnection.DefaultTimeout;

        /// <summary>
        /// Is debug only.
        /// </summary>
        public bool IsDebug;

        /// <summary>
        /// String dump of request for debugging.
        /// </summary>
        public string RequestDump;

        /// <summary>
        /// String dump of response for debugging.
        /// </summary>
        public string ResponseDump;
        private string _xmlVersionTag = "";

        protected const string ContentTypeXmlCharsetUtf8 = "Text/xml;charset=utf-8";
        protected const string ContentTypeSoapXmlCharsetUtf8 = "application/soap+xml;charset=utf-8";
        protected const string UserAgent = "ksoap2-android/2.6.0+";
    
        private readonly int _bufferLength = ServiceConnection.DefaultBufferSize;
        private readonly Dictionary<object, object> _prefixes = new Dictionary<object, object>();

        public Dictionary<object, object> GetPrefixes()
        {
            return _prefixes;
        }

        protected Transport() { }

        protected Transport(string url) : this(null, url) { }

        protected Transport(string url, int timeout)
        {
            Url = url;
            Timeout = timeout;
        }

        protected Transport(string url, int timeout, int bufferLength)
        {
            Url = url;
            Timeout = timeout;

            _bufferLength = bufferLength;
        }

        /// <summary>
        /// Construct the transport object.
        /// </summary>
        protected Transport(IWebProxy proxy, string url)
        {
            Proxy = proxy;
            Url = url;
        }

        protected Transport(IWebProxy proxy, string url, int timeout)
        {
            Proxy = proxy;
            Url = url;
            Timeout = timeout;
        }

        protected Transport(IWebProxy proxy, string url, int timeout, int bufferLength)
        {
            Proxy = proxy;
            Url = url;
            Timeout = timeout;
            _bufferLength = bufferLength;
        }

        /// <summary>
        /// Sets up the parsing to hand over to the envelope to deserialize.
        /// </summary>
        public Stream ToStream(string str)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        protected void ParseResponse(SoapEnvelope envelope, Stream inputStream)
        {
            if (envelope.ParseResponseAsString)
            {
                ParseReponseAsString(envelope, inputStream);
                return;
            }

            var xp = new FXmlParser();
            xp.SetFeature(XmlPullParser.FeatureProcessNamespaces, true);
            xp.SetInput(inputStream, null);
            envelope.Parse(xp);
            inputStream.Dispose();
        }

        protected void ParseReponseAsString(SoapEnvelope envelope, Stream inputStream)
        {
            var reader = new StreamReader(inputStream);
            envelope.Parse(reader.ReadToEnd());
            inputStream.Dispose();
        }

        /// <summary>
        /// Serializes the request.
        /// </summary>
        protected byte[] CreateRequestData(SoapEnvelope envelope, string encoding)
        {
            var e = Encoding.GetEncoding(encoding);
            var bos = new MemoryStream(_bufferLength);

            var tagBytes = Encoding.UTF8.GetBytes(_xmlVersionTag);
            bos.Write(tagBytes, 0, tagBytes.Length);
            var xw = new FXmlSerializer();

            xw.SetOutput(bos, e);
            foreach (var keyValuePair in _prefixes)
            {
                var key = (string)keyValuePair.Key;
                xw.SetPrefix(key, (string)keyValuePair.Value);
            }
            envelope.Write(xw);
            xw.Flush();
            bos.WriteByte((byte) '\r');
            bos.WriteByte((byte) '\n');
            bos.Flush();
            var result = bos.ToArray();
            xw = null;
            bos = null;
            return result;
        }

        /// <summary>
        /// Serializes the request.
        /// </summary>
        protected byte[] CreateRequestData(SoapEnvelope envelope)
        {
            return CreateRequestData(envelope, null);
        }

        /// <summary>
        /// Set the target url.
        /// </summary>
        public void SetUrl(string url)
        {
            Url = url;
        }

        public string GetUrl()
        {
            return Url;
        }

        /// <summary>
        /// Sets the version tag for the outgoing soap call.
        /// </summary>
        public void SetXmlVersionTag(string tag)
        {
            _xmlVersionTag = tag;
        }

        /// <summary>
        /// Attempts to reset the connection.
        /// </summary>
        public void Reset() { }

        /// <summary>
        /// Perform a soap call with a given namespace and the given envelope
        /// providing any extra headers that the user requires such as cookies.
        /// Headers that are returned by the web service will be returned to the
        /// caller in the form of an instances.
        /// </summary>
        public abstract List<HeaderProperty> Call(string soapAction, SoapEnvelope envelope, List<HeaderProperty> headers);

        /// <summary>
        /// Perform a soap call with a given namespace and the given envelope
        /// providing any extra headers that the user requires such as cookies.
        /// Headers that are returned by the web service will be returned to the
        /// caller in the form of an instances.
        /// </summary>
        public abstract List<HeaderProperty> Call(string soapAction, SoapEnvelope envelope, List<HeaderProperty> headers, FileStream outputFile);

        /// <summary>
        /// Perform a soap call with a given namespace and the given envelope.
        /// </summary>
        public virtual void Call(string soapAction, SoapEnvelope envelope)
        {
            Call(soapAction, envelope, null);
        }

        /// <summary>
        /// Return the name of the host that is specified as the web service target.
        /// </summary>
        public string GetHost()
        {
            return new Uri(Url).Host;
        }

        /// <summary>
        /// Return the port number of the host that is specified as the web service target.
        /// </summary>
        public int GetPort()
        {
            return new Uri(Url).Port;
        }

        /// <summary>
        /// Return the path to the web service target.
        /// </summary>
        public string GetPath()
        {
            return new Uri(Url).AbsolutePath;
        }

        public abstract ServiceConnection GetServiceConnection();
    }
}
