﻿namespace kSOAPXamarin.Soap
{
    public class HeaderProperty
    {
        private string _key;
        private string _value;

        public HeaderProperty(string key, string value)
        {
            _key = key;
            _value = value;
        }

        public string GetKey() => _key;
        public void SetKey(string key) => _key = key;
        public string GetValue() => _value;
        public void SetValue(string value) => _value = value;
    }
}
