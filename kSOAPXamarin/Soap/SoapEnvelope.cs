﻿using kSOAPXamarin.Xml.Dom;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap
{
    /// <summary>
    /// A SOAP envelope, holding head and body objects. While this basic envelope
    /// supports literal encoding as content format via KDom, The
    /// SoapSerializationEnvelope provides support for the SOAP Serialization format
    /// specification and simple object serialization.
    /// </summary>
    public class SoapEnvelope
    {
        /// <summary>
        /// Soap 1.0.
        /// </summary>
        public const int Ver10 = 100;

        /// <summary>
        /// Soap 1.1.
        /// </summary>
        public const int Ver11 = 110;
        
        /// <summary>
        /// Soap 1.2.
        /// </summary>
        public const int Ver12 = 120;

        public const string Env2003 = "http://www.w3.org/2003/05/soap-envelope";
        public const string Enc2003 = "http://www.w3.org/2003/05/soap-encoding";
        public const string Env = "http://schemas.xmlsoap.org/soap/envelope/";
        public const string Enc = "http://schemas.xmlsoap.org/soap/encoding/";
        public const string Xsd = "http://www.w3.org/2001/XMLSchema";
        public const string Xsi = "http://www.w3.org/2001/XMLSchema-instance";
        public const string Xsd1999 = "http://www.w3.org/1999/XMLSchema";  
        public const string Xsi1999 = "http://www.w3.org/1999/XMLSchema-instance";

        /// <summary>
        /// Returns true for the string values "1" and "true", ignoring upper/lower case and whitespace, false otherwise.
        /// </summary>
        public static bool StringToBoolean(string boolAsString)
        {
            if (boolAsString == null)
            {
                return false;
            }
            boolAsString = boolAsString.Trim().ToLower();
            return (boolAsString.Equals("1") || boolAsString.Equals("true"));
        }

        /// <summary>
        /// The body object received with this envelope. Will be an KDom Node for
        /// literal encoding. For SOAP Serialization, please refer to SoapSerializationEnvelope.
        /// </summary>
        public object BodyIn;

        /// <summary>
        /// The body object to be sent with this envelope. Must be a KDom Node
        /// modelling the remote call including all parameters for literal encoding.
        /// For SOAP Serialization, please refer to SoapSerializationEnvelope
        /// </summary>
        public object BodyOut;

        /// <summary>
        /// Incoming header elements.
        /// </summary>
        public Element[] HeaderIn;

        /// <summary>
        /// Outgoing header elements.
        /// </summary>
        public Element[] HeaderOut;
        public string EncodingStyle;

        #region Properties sets by the constructor
        public int Version;

        /// <summary>
        /// Envelope namespace.
        /// </summary>
        public string Envelope;

        /// <summary>
        /// Encoding namespace.
        /// </summary>
        public string Encoding;

        /// <summary>
        /// Xml Schema instance namespace.
        /// </summary>
        public string XmlSchemaInstance;

        /// <summary>
        /// Xml Schema data namespace.
        /// </summary>
        public string XmlSchemaData;
        #endregion

        public bool ParseResponseAsString = false;

        /// <summary>
        /// Initializes a SOAP Envelope. The version parameter must be set to one of
        /// VER10, VER11 or VER12.
        /// </summary>
        public SoapEnvelope(int version)
        {
            Version = version;
            if (version == Ver10)
            {
                XmlSchemaInstance = Xsi1999;
                XmlSchemaData = Xsd1999;
            }
            else
            {
                XmlSchemaInstance = Xsi;
                XmlSchemaData = Xsd;
            }
            if (version < Ver12)
            {
                Encoding = Enc;
                Envelope = Env;
            }
            else
            {
                Encoding = Enc2003;
                Envelope = Env2003;
            }
        }

        /// <summary>
        /// Parses the SOAP envelope from the given parser.
        /// </summary>
        public void Parse(XmlPullParser parser)
        {
            parser.NextTag();
            parser.Require(XmlPullParser.StartTag, Envelope, "Envelope");
            EncodingStyle = parser.GetAttributeValue(Envelope, "encodingStyle");
            parser.NextTag();
            if (parser.GetEventType() == XmlPullParser.StartTag
                && parser.GetNamespace().Equals(Envelope)
                && parser.GetName().Equals("Header"))
            {
                ParseHeader(parser);
                parser.Require(XmlPullParser.EndTag, Envelope, "Header");
                parser.NextTag();
            }
            parser.Require(XmlPullParser.StartTag, Envelope, "Body");
            EncodingStyle = parser.GetAttributeValue(Envelope, "encodingStyle");
            ParseBody(parser);
            parser.Require(XmlPullParser.EndTag, Envelope, "Body");
            parser.NextTag();
            parser.Require(XmlPullParser.EndTag, Envelope, "Envelope");
        }

        public void Parse(string text)
        {
            BodyIn = text;
        }

        public void ParseHeader(XmlPullParser parser)
        {
            parser.NextTag();

            var headers = new Node();
            headers.Parse(parser);
            var count = 0;
            for (var i = 0; i < headers.GetChildCount(); i++)
            {
                var child = headers.GetElement(i);
                if (child != null)
                {
                    count++;
                }
            }
            HeaderIn = new Element[count];
            count = 0;
            for (var i = 0; i < headers.GetChildCount(); i++)
            {
                var child = headers.GetElement(i);
                if (child != null)
                {
                    HeaderIn[count++] = child;
                }
            }
        }

        public virtual void ParseBody(XmlPullParser parser)
        {
            parser.NextTag();
           
            if (parser.GetEventType() == XmlPullParser.StartTag && parser.GetNamespace().Equals(Envelope) && parser.GetName().Equals("Fault"))
            {
                var fault = Version < Ver12 ? new SoapFault(Version) : new SoapFault12(Version);
                fault.Parse(parser);
                BodyIn = fault;
            }
            else
            {
                var node = BodyIn is Node node1 ? node1 : new Node();
                node.Parse(parser);
                BodyIn = node;
            }
        }

        /// <summary>
        /// Writes the complete envelope including header and body elements to the given XML writer.
        /// </summary>
        public void Write(IXmlSerializer writer)
        {
            writer.SetPrefix("i", XmlSchemaInstance);
            writer.SetPrefix("d", XmlSchemaData);
            writer.SetPrefix("c", Encoding);
            writer.SetPrefix("v", Envelope);
            writer.StartTag(Envelope, "Envelope");
            writer.StartTag(Envelope, "Header");
            WriteHeader(writer);
            writer.EndTag(Envelope, "Header");
            writer.StartTag(Envelope, "Body");
            WriteBody(writer);
            writer.EndTag(Envelope, "Body");
            writer.EndTag(Envelope, "Envelope");
        }

        /// <summary>
        /// Writes the header elements contained in headerOut.
        /// </summary>
        public void WriteHeader(IXmlSerializer writer)
        {
            if (HeaderOut == null)
                return;

            foreach (var element in HeaderOut)
            {
                element.Write(writer);
            }
        }

        /// <summary>
        /// Writes the SOAP body stored in the object variable bodyIn, Overwrite this method for customized writing of the soap message body.
        /// </summary>
        public virtual void WriteBody(IXmlSerializer writer)
        {
            if (EncodingStyle != null)
            {
                writer.Attribute(Envelope, "encodingStyle", EncodingStyle);
            }
            ((Node) BodyOut).Write(writer);
        }

        /// <summary>
        /// Assigns the object to the envelope as the outbound message for the soap call, soapObject the object to send in the soap call.
        /// </summary>
        public void SetOutputSoapObject(object soapObject)
        {
            BodyOut = soapObject;
        }

    }
}
