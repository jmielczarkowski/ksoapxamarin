﻿using System.Collections.Generic;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Provides get and set methods for properties. Can be used to replace
    /// reflection (to some extend) for "serialization-aware" classes. Currently used
    /// in kSOAP and the RMS based object repository.
    /// </summary>
    public interface IFvmSerializable
    {
        /// <summary>
        /// Get the property at the given index.
        /// </summary>
        object GetProperty(int index);

        /// <summary>
        /// Get number of properties.
        /// </summary>
        /// <returns></returns>
        int GetPropertyCount();

        /// <summary>
        /// Sets the property with the given index to the given value.
        /// </summary>
        void SetProperty(int index, object value);

        /// <summary>
        /// Fills the given property info record.
        /// </summary>
        void GetPropertyInfo(int index, Dictionary<object, object> properties, PropertyInfo info);
    }
}
