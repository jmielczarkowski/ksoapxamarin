﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// This class is used to store information about each property an implementation of KvmSerializable exposes.
    /// </summary>
    public class PropertyInfo
    {
        public static readonly Type ObjectClass = new object().GetType();
        public static readonly Type StringClass = "".GetType();
        public static readonly Type IntegerClass = new int().GetType();
        public static readonly Type LongClass = new long().GetType();
        public static readonly Type BooleanClass = new bool().GetType();
        public static readonly Type VectorClass = new List<object>().GetType();
        public static readonly PropertyInfo ObjectType = new PropertyInfo();
        public static readonly int Transient = 1;
        public static readonly int MultiRef = 2;
        public static readonly int RefOnly = 4;

        /// <summary>
        /// Name of the property.
        /// </summary>
        public string Name;

        /// <summary>
        /// Namespace of this property
        /// </summary>
        public string Namespace;

        /// <summary>
        /// Type of property, Transient, multi_ref, Ref_only *JHS* Note, not really used that effectively
        /// </summary>
        public int Flags;

        /// <summary>
        /// The current value of this property.
        /// </summary>
        protected internal object Value;

        /// <summary>
        /// Type of the property/elements. Should usually be an instance of Class.
        /// </summary>
        public object Type = ObjectClass;

        /// <summary>
        /// If a property is multi-referenced, set this flag to true.
        /// </summary>
        public bool MultiReference;

        /// <summary>
        /// Element type for array properties, null if not array prop.
        /// </summary>
        public PropertyInfo ElementType;

        public void Clear()
        {
            Type = ObjectClass;
            Flags = 0;
            Name = null;
            Namespace = null;
        }

        /// <summary>
        /// Returns the elementType.
        /// </summary>
        public PropertyInfo GetElementType()
        {
            return ElementType;
        }

        /// <summary>
        /// The elementType to set.
        /// </summary>
        public void SetElementType(PropertyInfo elementType)
        {
            ElementType = elementType;
        }

        /// <summary>
        /// Returns the flags.
        /// </summary>
        public int GetFlags()
        {
            return Flags;
        }

        /// <summary>
        /// The flags to set.
        /// </summary>
        public void SetFlags(int flags)
        {
            Flags = flags;
        }

        /// <summary>
        /// Returns the multiRef.
        /// </summary>
        public bool IsMultiRef()
        {
            return MultiReference;
        }

        /// <summary>
        /// The multiRef to set.
        /// </summary>
        public void SetMultiRef(bool multiReference)
        {
            MultiReference = multiReference;
        }

        /// <summary>
        /// Returns the name.
        /// </summary>
        public string GetName()
        {
            return Name;
        }

        /// <summary>
        /// The name to set.
        /// </summary>
        public void SetName(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Returns the namespace.
        /// </summary>
        public string GetNamespace()
        {
            return Namespace;
        }

        /// <summary>
        /// The namespace to set.
        /// </summary>
        public void SetNamespace(string _namespace)
        {
            Namespace = _namespace;
        }

        /// <summary>
        /// Returns the type.
        /// </summary>
#pragma warning disable 108,114
        public object GetType()
#pragma warning restore 108,114
        {
            return Type;
        }

        /// <summary>
        /// The type to set.
        /// </summary>
        public void SetType(object type)
        {
            Type = type;
        }

        /// <summary>
        /// Returns the value.
        /// </summary>
        public object GetValue()
        {
            return Value;
        }

        /// <summary>
        /// The value to set.
        /// </summary>
        public void SetValue(object value)
        {
            Value = value;
        }

        /// <summary>
        /// Show the name and value.
        /// </summary>
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(Name);
            stringBuilder.Append(" : ");
            if (Value != null)
                stringBuilder.Append(Value);
            else
                stringBuilder.Append("(not set)");

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Make a deep clone of the properties through object serialization.
        /// </summary>
        public object Clone()
        {
            var serializer = new DataContractSerializer(typeof (PropertyInfo));
            var stringBuilder = new StringBuilder();
            var writer = XmlWriter.Create(stringBuilder);
            serializer.WriteObject(writer, this);
            writer.Flush();
            writer.Dispose();
            var xml = stringBuilder.ToString();
            var obj = serializer.ReadObject(XmlReader.Create(new StringReader(xml)));

            return obj;
        }
    }
}
