﻿using System;
using System.Diagnostics;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Internal class.
    /// </summary>
    internal class DM : IMarshal
    {
        public object ReadInstance(XmlPullParser parser, string namespace_, string name, PropertyInfo excepted)
        {
            var text = parser.NextText();
            switch (name[0])
            {
                case 's':
                    return text;
                case 'i':
                    return int.Parse(text);
                case 'l':
                    return long.Parse(text);
                case 'b':
                    return SoapEnvelope.StringToBoolean(text);
                default:
                    throw new Exception();
            }
        }

        /// <summary>
        /// Write the instance out. In case it is an AttributeContainer Write those our first though. 
        /// If it HasAttributes then Write the attributes and values.
        /// </summary>
        public void WriteInstance(IXmlSerializer writer, object instance)
        {
            switch (instance)
            {
                case AttributeContainer attributeContainer:
                {
                    var cnt = attributeContainer.GetAttributeCount();
                    for (var counter = 0; counter < cnt; counter++)
                    {
                        var attributeInfo = new AttributeInfo();
                        attributeContainer.GetAttributeInfo(counter, attributeInfo);
                        try
                        {
                            attributeContainer.GetAttribute(counter, attributeInfo);
                        }
                        catch (Exception exception)
                        {
                            Debug.WriteLine($"{exception}");
                        }
                        if (attributeInfo.GetValue() != null)
                        {
                            writer.Attribute(attributeInfo.GetNamespace(), attributeInfo.GetName(),
                                attributeInfo.GetValue() != null ? attributeInfo.GetValue().ToString() : "");
                        }
                    }

                    break;
                }
                case IHasAttributes soapObject:
                {
                    var cnt = soapObject.GetAttributeCount();
                    for (var counter = 0; counter < cnt; counter++)
                    {
                        var attributeInfo = new AttributeInfo();
                        soapObject.GetAttributeInfo(counter, attributeInfo);
                        try
                        {
                            soapObject.GetAttribute(counter, attributeInfo);
                        }
                        catch (Exception exception)
                        {
                            Debug.WriteLine($"{exception}");
                        }
                        if (attributeInfo.GetValue() != null)
                        {
                            writer.Attribute(attributeInfo.GetNamespace(), attributeInfo.GetName(),
                                attributeInfo.GetValue() != null ? attributeInfo.GetValue().ToString() : "");
                        }
                    }

                    break;
                }
            }

            writer.Text(instance.ToString());
        }
        public void Register(SoapSerializationEnvelope cm)
        {
            cm.AddMapping(cm.XmlSchemaData, "int", PropertyInfo.IntegerClass, this);
            cm.AddMapping(cm.XmlSchemaData, "long", PropertyInfo.LongClass, this);
            cm.AddMapping(cm.XmlSchemaData, "string", PropertyInfo.StringClass, this);
            cm.AddMapping(cm.XmlSchemaData, "boolean", PropertyInfo.BooleanClass, this);
        }
    }
}