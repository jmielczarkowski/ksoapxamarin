﻿namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Forwarded reference implementation.
    /// </summary>
    internal class FwdRef
    {
        internal FwdRef Next;
        internal object Obj;
        internal int Index;
    }
}
