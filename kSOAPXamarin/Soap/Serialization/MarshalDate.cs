﻿using System;
using kSOAPXamarin.Entities;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Marshal class for dates. 
    /// </summary>
    public class MarshalDate : IMarshal
    {
        public static Type DateClass = new DateTime().GetType();

        public object ReadInstance(XmlPullParser parser, string namespace_, string name, PropertyInfo expected)
        {
            return IsoDate.StringToDate(parser.NextText(), IsoDate.DateTime);
        }

        public void WriteInstance(IXmlSerializer writer, object obj)
        {
            writer.Text(IsoDate.DateToString((DateTime)obj, IsoDate.DateTime));
        }

        public void Register(SoapSerializationEnvelope cm)
        {
            cm.AddMapping(cm.XmlSchemaData, "dateTime", DateClass, this);
        }
    }
}
