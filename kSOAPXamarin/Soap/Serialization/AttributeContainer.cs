﻿using System;
using System.Collections.Generic;

namespace kSOAPXamarin.Soap.Serialization
{
    public class AttributeContainer : IHasAttributes
    {
        protected List<object> Attributes = new List<object>();

        public void GetAttributeInfo(int index, AttributeInfo attributeInfo)
        {
            var p = (AttributeInfo)Attributes[index];
            attributeInfo.Name = p.Name;
            attributeInfo.Namespace = p.Namespace;
            attributeInfo.Flags = p.Flags;
            attributeInfo.Type = p.Type;
            attributeInfo.ElementType = p.ElementType;
            attributeInfo.Value = p.GetValue();
        }

        /// <summary>
        /// Get the Attribute at the given index.
        /// </summary>
        public object GetAttribute(int index)
        {
            return ((AttributeInfo)Attributes[index]).GetValue();
        }

        /// <summary>
        /// Get the Attribute's toString value.
        /// </summary>
        public string GetAttributeAsString(int index)
        {
            var attributeInfo = (AttributeInfo)Attributes[index];
            return attributeInfo.GetValue().ToString();
        }

        /// <summary>
        /// Get the Attribute with the given name.
        /// </summary>
        public object GetAttribute(string name)
        {
            var i = AttributeIndex(name);
            if (i != null)
                return GetAttribute(i.Value);

            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Get the Attribute with the given name.
        /// </summary>
        public object GetAttribute(string _namespace, string name)
        {
            var i = AttributeIndex(_namespace, name);
            if (i != null)
                return GetAttribute(i.Value);

            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Get the toString value of the Attribute with the given name.
        /// </summary>
        public string GetAttributeAsString(string name)
        {
            var i = AttributeIndex(name);
            if (i != null)
                return GetAttribute(i.Value).ToString();

            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Get the toString value of the Attribute with the given name.
        /// </summary>
        public string GetAttributeAsString(string _namespace, string name)
        {
            var i = AttributeIndex(_namespace, name);
            if (i != null)
                return GetAttribute(i.Value).ToString();

            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Knows whether the given Attribute exists.
        /// </summary>
        public bool HasAttribute(string name)
        {
            return AttributeIndex(name) != null;
        }

        /// <summary>
        /// Knows whether the given Attribute exists.
        /// </summary>
        public bool HasAttribute(string _namespace, string name)
        {
            return AttributeIndex(_namespace, name) != null;
        }

        /// <summary>
        /// Get an Attribute without chance of throwing an exception.
        /// </summary>
        public object GetAttributeSafely(string name)
        {
            var i = AttributeIndex(name);
            return i != null ? GetAttribute(i.Value) : null;
        }

        /// <summary>
        /// Get an Attribute without chance of throwing an exception
        /// </summary>
        public object GetAttributeSafely(string _namespace, string name)
        {
            var i = AttributeIndex(_namespace, name);
            return i != null ? GetAttribute(i.Value) : null;
        }

        /// <summary>
        /// Get an attributes' toString value without chance of throwing an exception.
        /// </summary>
        public object GetAttributeSafelyAsString(string name)
        {
            var i = AttributeIndex(name);
            return i != null ? GetAttribute(i.Value).ToString() : "";
        }

        /// <summary>
        /// Get an attributes' toString value without chance of throwing an exception.
        /// </summary>
        public object GetAttributeSafelyAsString(string _namespace, string name)
        {
           var i = AttributeIndex(_namespace, name);
            return i != null ? GetAttribute(i.Value).ToString() : "";
        }

        private int? AttributeIndex(string name)
        {
            for (var i = 0; i < Attributes.Count; i++)
            {
                if (name.Equals(((AttributeInfo)Attributes[i]).GetName()))
                    return i;
            }
            return null;
        }

        private int? AttributeIndex(string _namespace, string name)
        {
            for (var i = 0; i < Attributes.Count; i++)
            {
                var attrInfo = (AttributeInfo)Attributes[i];
                if (name.Equals(attrInfo.GetName()) && _namespace.Equals(attrInfo.GetNamespace()))
                    return i;
            }
            return null;
        }

        /// <summary>
        /// Returns the number of attributes.
        /// </summary>
        public int GetAttributeCount()
        {
            return Attributes.Count;
        }

        /// <summary>
        /// Checks that the two objects have identical sets of attributes.
        /// </summary>
        protected bool AttributesAreEqual(AttributeContainer other)
        {
            var numAttributes = GetAttributeCount();
            if (numAttributes != other.GetAttributeCount())
            {
                return false;
            }

            for (var attribIndex = 0; attribIndex < numAttributes; attribIndex++)
            {
                var thisAttrib = (AttributeInfo) Attributes[attribIndex];
                var thisAttribValue = thisAttrib.GetValue();
                if (!other.HasAttribute(thisAttrib.GetName()))
                    return false;

                var otherAttribValue = other.GetAttributeSafely(thisAttrib.GetName());
                if (!thisAttribValue.Equals(otherAttribValue))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Adds a Attribute (parameter) to the object.
        /// </summary>
        public void AddAttribute(string name, object value)
        {
            AddAttribute(null, name, value);
        }

        /// <summary>
        /// Adds a Attribute (parameter) to the object.
        /// </summary>
        public void AddAttribute(string _namespace, string name, object value)
        {
            var attributeInfo = new AttributeInfo
            {
                Name = name,
                Namespace = _namespace,
                Type = value == null ? PropertyInfo.ObjectClass : value.GetType(),
                Value = value
            };
            AddAttribute(attributeInfo);
        }

        /// <summary>
        /// Add an Attribute if the value is not null.
        /// </summary>
        public void AddAttributeIfValue(string name, object value)
        {
            if (value != null)
                AddAttribute(name, value);
        }

        /// <summary>
        /// Add an Attribute if the value is not null.
        /// </summary>
        public void AddAttributeIfValue(string _namespace, string name, object value)
        {
            if (value != null)
                AddAttribute(_namespace, name, value);
        }

        /// <summary>
        /// Add a new Attribute by providing an object.
        /// Contains all data about the Attribute, including name and value.
        /// </summary>
        public void AddAttribute(AttributeInfo attributeInfo)
        {
            Attributes.Add(attributeInfo);
        }

        /// <summary>
        /// Add an attributeInfo if its value is not null.
        /// </summary>
        public void AddAttributeIfValue(AttributeInfo attributeInfo)
        {
            if (attributeInfo.Value != null)
                Attributes.Add(attributeInfo);
        }

        /// <summary>
        /// Throws not implemented exception.
        /// </summary>
        public void GetAttribute(int index, AttributeInfo info)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Throws not implemented exception.
        /// </summary>
        public void SetAttribute(AttributeInfo info)
        {
            throw new NotImplementedException();
        }
    }
}
