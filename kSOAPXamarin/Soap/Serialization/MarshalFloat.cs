﻿using System;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    public class MarshalFloat : IMarshal
    {
        public object ReadInstance(XmlPullParser parser, string _namespace, string name, PropertyInfo propertyInfo)
        {
            var stringValue = parser.NextText();
            object result;
            if (name.Equals("float"))
            {
                result = float.Parse(stringValue);
            }
            else if (name.Equals("double"))
            {
                result = double.Parse(stringValue);
            }
            else if (name.Equals("decimal"))
            {
                result = decimal.Parse(stringValue);
            }
            else
            {
                throw new Exception("float, double, or decimal expected.");
            }

            return result;
        }

        public void WriteInstance(IXmlSerializer writer, object instance)
        {
            writer.Text(instance.ToString());
        }

        public void Register(SoapSerializationEnvelope cm)
        {
            cm.AddMapping(cm.XmlSchemaData, "float", typeof(float), this);
            cm.AddMapping(cm.XmlSchemaData, "double", typeof(double), this);
            cm.AddMapping(cm.XmlSchemaData, "decimal", typeof(decimal), this);
        }
    }
}
