﻿using System;
using kSOAPXamarin.Entities;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Base64 (de)serializer.
    /// </summary>
    public class MarshalBase64 : IMarshal
    {
        public static Type ByteArrayClass = new byte[0].GetType();

        public object ReadInstance(XmlPullParser parser, string _namespace, string name, PropertyInfo expected)
        {
            return Base64.Decode(parser.NextText());
        }

        public void WriteInstance(IXmlSerializer writer, object obj)
        {
            writer.Text(Base64.Encode((byte[])obj));
        }

        public void Register(SoapSerializationEnvelope cm)
        {
            cm.AddMapping(cm.XmlSchemaData, "base64Binary", ByteArrayClass, this);
            cm.AddMapping(SoapEnvelope.Enc, "base64", ByteArrayClass, this);
        }
    }
}
