﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kSOAPXamarin.Soap.Serialization
{
    public class SoapObject : AttributeContainer, IFvmSerializable, IHasInnerText
    {
        private static readonly string EmptyString = "";
        
        /// <summary>
        /// The _Namespace of this soap object.
        /// </summary>
        protected internal string Namespace;

        /// <summary>
        /// The name of this soap object.
        /// </summary>
        protected internal string Name;

        /// <summary>
        /// The Vector of properties (can contain PropertyInfo and SoapObject).
        /// </summary>
        protected List<object> Properties = new List<object>();

        protected string InnerText;

        public SoapObject() : this("", "")
        {
        }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        public SoapObject(string _namespace, string name)
        {
            Namespace = _namespace;
            Name = name;
        }

#pragma warning disable 659
        public override bool Equals(object obj)
#pragma warning restore 659
        {
            if (!(obj is SoapObject))
                return false;

            var otherSoapObject = (SoapObject) obj;

            if (!Name.Equals(otherSoapObject.Name) || !Namespace.Equals(otherSoapObject.Namespace))
                return false;

            var numProperties = Properties.Count;
            if (numProperties != otherSoapObject.Properties.Count)
                return false;

            // SoapObjects are only considered the same if properties Equals and in the same order.
            for (var propIndex = 0; propIndex < numProperties; propIndex++)
            {
                var thisProp = Properties[propIndex];
                if (!otherSoapObject.IsPropertyEqual(thisProp, propIndex))
                    return false;
            }

            return AttributesAreEqual(otherSoapObject);
        }

        /// <summary>
        /// Helper function for SoapObject.Equals,
        /// Checks if a given property and index are the same as in this.
        /// </summary>
        public bool IsPropertyEqual(object otherProp, int index)
        {
            if (index >= GetPropertyCount())
                return false;
            
            var thisProp = Properties[index];
            if (otherProp is PropertyInfo otherPropInfo && thisProp is PropertyInfo thisPropInfo)
                return otherPropInfo.GetName().Equals(thisPropInfo.GetName()) &&
                       otherPropInfo.GetValue().Equals(thisPropInfo.GetValue());

            if (otherProp is SoapObject otherPropSoap && thisProp is SoapObject thisPropSoap)
                return otherPropSoap.Equals(thisPropSoap);
            
            return false;
        }

        public string GetName() => Name;

        public string GetNamespace() => Namespace;

        public object GetProperty(int index)
        {
            var prop = Properties[index];
            if (prop is PropertyInfo info)
            {
                return info.GetValue();
            }

            return (SoapObject) prop;
        }

        /// <summary>
        /// Get the ToString value of the property.
        /// </summary>
        public string GetPropertyAsString(int index)
        {
            var propertyInfo = (PropertyInfo) Properties[index];
            return propertyInfo.GetValue().ToString();
        }

        /// <summary>
        /// Get the property with the given name.
        /// </summary>
        public object GetProperty(string name)
        {
            var index = PropertyIndex(name);
            if (index != null)
            {
                return GetProperty(index.Value);
            }

            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Get a property using _Namespace and name without chance of throwing an exception.
        /// </summary>
        public object GetPropertyByNamespaceSafely(string _namespace, string name)
        {
            var i = PropertyIndex(_namespace, name);
            return i != null ? GetProperty(i.Value) : new NullSoapObject();
        }

        /// <summary>
        /// Get the ToString value of a property without chance of throwing an exception.
        /// </summary>
        public string GetPropertyByNamespaceSafelyAsString(string _namespace, string name)
        {
            var i = PropertyIndex(_namespace, name);
            if (i != null)
            {
                var foo = GetProperty(i.Value);
                return foo == null ? EmptyString : foo.ToString();
            }

            return EmptyString;
        }

        /// <summary>
        /// Get a property without chance of throwing an exception. An object can be
        /// provided to this method; if the property is not found, this object will
        /// be returned.
        /// </summary>
        public object GetPropertySafely(string _namespace, string name, object defaultThing)
        {
            var i = PropertyIndex(_namespace, name);
            return i != null ? GetProperty(i.Value) : defaultThing;
        }

        /// <summary>
        /// Get the ToString value of a property without chance of throwing an
        /// exception. An object can be provided to this method; if the property is
        /// not found, this object's string representation will be returned.
        /// </summary>
        public string GetPropertySafelyAsString(string _namespace, string name, object defaultThing)
        {
            var i = PropertyIndex(_namespace, name);
            if (i == null)
                return defaultThing != null ? defaultThing.ToString() : EmptyString;

            var property = GetProperty(i.Value);
            return property != null ? property.ToString() : EmptyString;
        }

        /// <summary>
        /// Get the primitive property with the given name.
        /// </summary>
        public object GetPrimitiveProperty(string _namespace, string name)
        {
            var index = PropertyIndex(_namespace, name);
            if (index != null)
            {
                PropertyInfo propertyInfo = (PropertyInfo) Properties[index.Value];
                if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
                {
                    return propertyInfo.GetValue();
                }

                propertyInfo = new PropertyInfo();
                propertyInfo.SetType(typeof(string));
                propertyInfo.SetValue(EmptyString);
                propertyInfo.SetName(name);
                propertyInfo.SetNamespace(_namespace);
                return propertyInfo.GetValue();
            }

            throw new Exception($"Illegal property: {name}.");
        }

        /**
     * Get the ToString value of the primitive property with the given name.
     * Returns empty string if property either complex or empty
     *
     * @param name
     * @return the string value of the property
     */

        public string GetPrimitivePropertyAsString(string _namespace, string name)
        {
            var index = PropertyIndex(_namespace, name);
            if (index != null)
            {
                PropertyInfo propertyInfo = (PropertyInfo) Properties[index.Value];
                if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
                {
                    return propertyInfo.GetValue().ToString();
                }
                else
                {
                    return EmptyString;
                }
            }
            else
            {
                throw new Exception($"Illegal property: {name}.");
            }
        }

        /// <summary>
        /// Get the ToString value of a primitive property without chance of throwing an exception.
        /// </summary>
        public object GetPrimitivePropertySafely(string _namespace, string name)
        {
            var index = PropertyIndex(_namespace, name);
            if (index == null)
                return new NullSoapObject();

            var propertyInfo = (PropertyInfo) Properties[index.Value];
            if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
            {
                return propertyInfo.GetValue().ToString();
            }

            propertyInfo = new PropertyInfo();
            propertyInfo.SetType(typeof (string));
            propertyInfo.SetValue(EmptyString);
            propertyInfo.SetName(name);
            propertyInfo.SetNamespace(_namespace);
            return propertyInfo.GetValue();

        }

        /// <summary>
        ///  Get the ToString value of a primitive property without chance of throwing an exception.
        /// </summary>
        public string GetPrimitivePropertySafelyAsString(string _namespace, string name)
        {
            var index = PropertyIndex(_namespace, name);
            if (index != null)
            {
                var propertyInfo = (PropertyInfo) Properties[index.Value];
                if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
                {
                    return propertyInfo.GetValue().ToString();
                }

                return EmptyString;
            }

            return EmptyString;
        }

        /// <summary>
        /// Knows whether the given property exists.
        /// </summary>
        public bool HasProperty(string _namespace, string name)
        {
            if (PropertyIndex(_namespace, name) != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the ToString value of the property.
        /// </summary>
        public string GetPropertyAsString(string _namespace, string name)
        {
            var index = PropertyIndex(_namespace, name);
            return index != null
                ? GetProperty(index.Value).ToString()
                : throw new Exception($"Illegal property: {name}");
        }

        /// <summary>
        /// Get the ToString value of the property.
        /// </summary>
        public string GetPropertyAsString(string name)
        {
            var index = PropertyIndex(name);
            return index != null
                ? GetProperty(index.Value).ToString()
                : throw new Exception($"Illegal property: {name}");
        }

        /// <summary>
        /// Knows whether the given property exists.
        /// </summary>
        public bool HasProperty(string name)
        {
            return PropertyIndex(name) != null;
        }

        /**
     * Get a property without chance of throwing an exception
     *
     * @return the property if it exists; if not, {@link NullSoapObject} is
     *         returned
     */

        public object GetPropertySafely(string name)
        {
            var i = PropertyIndex(name);
            if (i != null)
            {
                return GetProperty(i.Value);
            }
            else
            {
                return new NullSoapObject();
            }
        }

        /**
     * Get the ToString value of a property without chance of throwing an
     * exception
     *
     * @return the string value of the property if it exists; if not, #EMPTY_STRING is
     *         returned
     */

        public string GetPropertySafelyAsString(string name)
        {
            int? i = PropertyIndex(name);
            if (i != null)
            {
                object foo = GetProperty(i.Value);
                if (foo == null)
                {
                    return EmptyString;
                }
                else
                {
                    return foo.ToString();
                }
            }
            else
            {
                return EmptyString;
            }
        }

        /**
     * Get a property without chance of throwing an exception. An object can be
     * provided to this method; if the property is not found, this object will
     * be returned.
     *
     * @param defaultThing
     *            the object to return if the property is not found
     * @return the property if it exists; defaultThing if the property does not
     *         exist
     */

        public object GetPropertySafely(string name, object defaultThing)
        {
            int? i = PropertyIndex(name);
            if (i != null)
            {
                return GetProperty(i.Value);
            }
            else
            {
                return defaultThing;
            }
        }

        /// <summary>
        /// Get the ToString value of a property without chance of throwing an
        /// exception. An object can be provided to this method; if the property is
        /// not found, this object's string representation will be returned.
        /// </summary>
        public string GetPropertySafelyAsString(string name, object defaultThing)
        {
            var i = PropertyIndex(name);
            if (i != null)
            {
                var property = GetProperty(i.Value);
                return property != null ? property.ToString() : EmptyString;
            }

            return defaultThing != null ? defaultThing.ToString() : EmptyString;
        }

        /// <summary>
        /// Get the primitive property with the given name.
        /// </summary>
        public object GetPrimitiveProperty(string name)
        {
            var index = PropertyIndex(name);
            if (index != null)
            {
                var propertyInfo = (PropertyInfo) Properties[index.Value];
                if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
                {
                    return propertyInfo.GetValue();
                }

                propertyInfo = new PropertyInfo();
                propertyInfo.SetType(typeof(string));
                propertyInfo.SetValue(EmptyString);
                propertyInfo.SetName(name);
                return propertyInfo.GetValue();
            }
            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Get the ToString value of the primitive property with the given name.
        /// Returns empty string if property either complex or empty
        /// </summary>
        public string GetPrimitivePropertyAsString(string name)
        {
            var index = PropertyIndex(name);
            if (index != null)
            {
                var propertyInfo = (PropertyInfo) Properties[index.Value];
                if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
                {
                    return propertyInfo.GetValue().ToString();
                }

                return EmptyString;
            }
            throw new Exception($"Illegal property: {name}.");
        }

        /// <summary>
        /// Get the ToString value of a primitive property without chance of throwing an exception.
        /// </summary>
        public object GetPrimitivePropertySafely(string name)
        {
            var index = PropertyIndex(name);
            if (index != null)
            {
                var propertyInfo = (PropertyInfo) Properties[index.Value];
                if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
                {
                    return propertyInfo.GetValue().ToString();
                }

                propertyInfo = new PropertyInfo();
                propertyInfo.SetType(typeof(string));
                propertyInfo.SetValue(EmptyString);
                propertyInfo.SetName(name);
                return propertyInfo.GetValue();
            }

            return new NullSoapObject();
        }

        /// <summary>
        /// Get the ToString value of a primitive property without chance of throwing an exception.
        /// </summary>
        public string GetPrimitivePropertySafelyAsString(string name)
        {
            var index = PropertyIndex(name);
            if (index == null)
                return EmptyString;

            var propertyInfo = (PropertyInfo) Properties[index.Value];
            if (!propertyInfo.GetType().Equals(typeof (SoapObject)) && propertyInfo.GetValue() != null)
            {
                return propertyInfo.GetValue().ToString();
            }

            return EmptyString;
        }

        private int? PropertyIndex(string name)
        {
            if (name == null)
                return null;

            for (var i = 0; i < Properties.Count; i++)
            {
                if (name.Equals(((PropertyInfo) Properties[i]).GetName()))
                    return i;
            }

            return null;
        }

        private int? PropertyIndex(string _namespace, string name)
        {
            if (name == null || _namespace == null) return null;
            for (var i = 0; i < Properties.Count; i++)
            {
                var info = (PropertyInfo) Properties[i];
                if (name.Equals(info.GetName()) && _namespace.Equals(info.GetNamespace()))
                {
                    return i;
                }
            }
            return null;
        }

        public int GetPropertyCount() => Properties.Count;

        public virtual void SetProperty(int index, object value)
        {
            var prop = Properties[index];
            if (prop is PropertyInfo info)
            {
                info.SetValue(value);
            }       
        }

        /// <summary>
        /// Adds a property (parameter) to the object. This is essentially a sub element.
        /// </summary>
        public SoapObject AddProperty(string name, object value)
        {
            var propertyInfo = new PropertyInfo
            {
                Name = name,
                Type = value == null
                    ? PropertyInfo.ObjectClass
                    : value
                        .GetType(),
                Value = value
            };
            return AddProperty(propertyInfo);
        }

        /// <summary>
        /// Adds a property (parameter) to the object. This is essentially a sub element.
        /// </summary>
        public SoapObject AddProperty(string _namespace, string name, object value)
        {
            var propertyInfo = new PropertyInfo
            {
                Name = name,
                Namespace = _namespace,
                Type = value == null
                    ? PropertyInfo.ObjectClass
                    : value
                        .GetType(),
                Value = value
            };
            return AddProperty(propertyInfo);
        }

        /// <summary>
        /// Add a property only if the value is not null.
        /// </summary>
        public SoapObject AddPropertyIfValue(string _namespace, string name, object value)
        {
            return value != null ? AddProperty(_namespace, name, value) : this;
        }

        /// <summary>
        /// Add a property only if the value is not null.
        /// </summary>
        public SoapObject AddPropertyIfValue(string name, object value)
        {
            return value != null ? AddProperty(name, value) : this;
        }

        /// <summary>
        /// Add a property only if the value is not null.
        /// </summary>
        public SoapObject AddPropertyIfValue(PropertyInfo propertyInfo, object value)
        {
            if (value == null)
                return this;

            propertyInfo.SetValue(value);
            return AddProperty(propertyInfo);

        }

        /// <summary>
        /// Adds a property (parameter) to the object. This is essentially a sub element.
        /// </summary>
        public SoapObject AddProperty(PropertyInfo propertyInfo)
        {
            Properties.Add(propertyInfo);
            return this;
        }

        /// <summary>
        /// Ad the propertyInfo only if the value of it is not null.
        /// </summary>
        public SoapObject AddPropertyIfValue(PropertyInfo propertyInfo)
        {
            if (propertyInfo.Value != null)
            {
                Properties.Add(propertyInfo);
                return this;
            }

            return this;
        }

        /// <summary>
        /// Adds a SoapObject the properties array. This is a sub element to
        /// allow nested SoapObjects.
        /// </summary>
        public SoapObject AddSoapObject(SoapObject soapObject)
        {
            Properties.Add(soapObject);
            return this;
        }

        public void GetPropertyInfo(int index, Dictionary<object, object> properties, PropertyInfo propertyInfo)
        {
            GetPropertyInfo(index, propertyInfo);
        }

        /// <summary>
        /// Places PropertyInfo of desired property into a designated PropertyInfo object.
        /// </summary>
        public void GetPropertyInfo(int index, PropertyInfo propertyInfo)
        {
            var element = Properties[index];
            if (element is PropertyInfo p)
            {
                propertyInfo.Name = p.Name;
                propertyInfo.Namespace = p.Namespace;
                propertyInfo.Flags = p.Flags;
                propertyInfo.Type = p.Type;
                propertyInfo.ElementType = p.ElementType;
                propertyInfo.Value = p.Value;
                propertyInfo.MultiReference = p.MultiReference;
            }
            else
            {
                propertyInfo.Name = null;
                propertyInfo.Namespace = null;
                propertyInfo.Flags = 0;
                propertyInfo.Type = null;
                propertyInfo.ElementType = null;
                propertyInfo.Value = element;
                propertyInfo.MultiReference = false;
            }
        }

        /// <summary>
        /// Creates a new SoapObject based on this, allows usage of SoapObjects as
        /// templates. One application is to set the expected return type of a soap
        /// call if the server does not send explicit type information.
        /// </summary>
        public SoapObject NewInstance()
        {
            var o = new SoapObject(Namespace, Name);
            foreach (var property in Properties)
            {
                var prop = property;
                if (prop is PropertyInfo)
                {
                    var propertyInfo = (PropertyInfo) property;
                    var propertyInfoClonned = (PropertyInfo) propertyInfo.Clone();
                    o.AddProperty(propertyInfoClonned);
                }
                else if (prop is SoapObject soapObject)
                {
                    o.AddSoapObject(soapObject.NewInstance());
                }
            }
            for (var attribIndex = 0; attribIndex < GetAttributeCount(); attribIndex++)
            {
                var newAttributeInfo = new AttributeInfo();
                GetAttributeInfo(attribIndex, newAttributeInfo);
                var attributeInfo = newAttributeInfo;
                o.AddAttribute(attributeInfo);
            }
            return o;
        }

        public override string ToString()
        {
            var buf = new StringBuilder(EmptyString + Name + "{");
            for (var i = 0; i < GetPropertyCount(); i++)
            {
                var prop = Properties[i];
                if (prop is PropertyInfo info)
                {
                    buf.Append(EmptyString)
                        .Append(info.GetName())
                        .Append("=")
                        .Append(GetProperty(i))
                        .Append("; ");
                }
                else
                {
                    buf.Append((SoapObject) prop);
                }
            }
            buf.Append("}");
            return buf.ToString();
        }

        public string GetInnerText() => InnerText;

        public void SetInnerText(string innerText) => InnerText = innerText;
    }

    /// <summary>
    /// This is useful in the case where you have a SoapObject representing an optional property in your SOAP response.
    /// </summary>
    public class NullSoapObject
    {
        /// <summary>
        /// Overridden specifically to always return null.
        /// </summary>
        public override string ToString() => string.Empty;
    }
}
