﻿
namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Interface for classes requiring inner Text of xml tags.
    /// </summary>
    public interface IHasInnerText
    {
        /// <summary>
        /// Gets the inner Text of xml tags.
        /// </summary>
        string GetInnerText();

        /// <summary>
        /// String to be set as inner Text for an outgoing soap object.
        /// </summary>
        void SetInnerText(string s);
    }
}
