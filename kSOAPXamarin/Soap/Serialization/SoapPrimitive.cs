﻿using System;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    ///  A class that is used to encapsulate primitive types (represented by a string in XML serialization).
    /// </summary>
    public class SoapPrimitive : AttributeContainer
    {
        // ReSharper disable once InconsistentNaming
        protected readonly string _Namespace;
        protected readonly string Name;
        protected readonly object Value;

        public static readonly object NullSkip = new object();
        public static readonly object NullNilElement = new object();

        public SoapPrimitive(string _namespace, string name, object value)
        {
            _Namespace = _namespace;
            Name = name;
            Value = value;
        }

        public override bool Equals(object o)
        {
            if (!(o is SoapPrimitive))
                return false;

            var p = (SoapPrimitive) o;
            var varsEqual = Name.Equals(p.Name)
                            && (_Namespace?.Equals(p._Namespace) ?? p._Namespace == null)
                            && (Value?.Equals(p.Value) ?? p.Value == null);

            return varsEqual && AttributesAreEqual(p);
        }

        public override int GetHashCode() => Name.GetHashCode() ^ (_Namespace == null ? 0 : _Namespace.GetHashCode());

        public override string ToString() => Value?.ToString() ?? 
                                             throw new InvalidOperationException();

        public string GetNamespace() => _Namespace;

        public string GetName() => Name;

        public object GetValue() => Value;
    }
}
