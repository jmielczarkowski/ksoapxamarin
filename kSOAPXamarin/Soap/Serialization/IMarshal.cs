﻿using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Interface for custom (de)serialization.
    /// </summary>
    public interface IMarshal
    {
        /// <summary>
        /// This methods reads an instance from the given parser. For implementation,
        /// please note that the start and and tag must be consumed. This is not
        /// symmetric to writeInstance, but otherwise it would not be possible to
        /// access the attributes of the start tag here.
        /// </summary>
        object ReadInstance(XmlPullParser parser, string _namespace, string name, PropertyInfo expected);

        /// <summary>
        /// Write the instance to the given IXmlSerializer. In contrast to
        /// readInstance, it is not necessary to care about the surrounding start
        /// and end tags. Additional attributes must be written before anything else
        /// is written.
        /// </summary>
        void WriteInstance(IXmlSerializer writer, object instance);

        /// <summary>
        /// Register this Marshal with Envelope.
        /// </summary>
        void Register(SoapSerializationEnvelope envelope);
    }
}