﻿namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Interface for classes required checking with has attributes info.
    /// </summary>
    public interface IHasAttributes
    {
        int GetAttributeCount();

        void GetAttributeInfo(int index, AttributeInfo info);

        void GetAttribute(int index, AttributeInfo info);

        void SetAttribute(AttributeInfo info);
    }
}
