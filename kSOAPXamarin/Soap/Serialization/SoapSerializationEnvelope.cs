﻿using System;
using System.Collections.Generic;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    public class SoapSerializationEnvelope : SoapEnvelope
    {
        protected static readonly int QNameType = 1;
        protected static readonly int QNameNamespace = 0;
        protected static readonly int QNameMarshal = 3;
        protected static readonly string NullLabel = "null";
        protected static readonly string NilLabel = "nil";
        private static readonly IMarshal DefaultMarshal = new DM();
        private const string AnyTypeLabel = "anyType";
        private const string ArrayMappingName = "Array";
        private const string HrefLabel = "href";
        private const string IdLabel = "id";
        private static readonly string RootLabel = "root";
        private static readonly string TypeLabel = "type";
        private static readonly string ItemLabel = "item";
        private static readonly string ArrayTypeLabel = "arrayType";
        public Dictionary<object, object> Properties = new Dictionary<object, object>();

        /// <summary>
        /// Set this variable to true if you don't want that type definitions for complex types/objects
        /// are automatically generated (with type "anyType") in the XML-Request, if you don't call the
        /// Method AddMapping. This is needed by some Servers which have problems with these type-definitions.
        /// </summary>
        public bool ImplicitTypes;

        /// <summary>
        /// If set to true then all properties with null value will be skipped from the soap message.
        /// If false then null properties will be sent as null.
        /// </summary>
        public bool SkipNullProperties;

        /// <summary>
        /// Set this variable to true for compatibility with what seems to be the default encoding for
        /// .Net-Services. This feature is an extremely ugly hack. A much better option is to change the
        /// configuration of the .Net-Server to standard Soap Serialization!
        /// </summary>
        public bool DotNet;

        /// <summary>
        /// Set this variable to true if you prefer to silently skip unknown properties.
        /// </summary>
        public bool AvoidExceptionForUnknownProperty;

        /// <summary>
        /// Map from XML qualified names to Java classes.
        /// </summary>
        protected Dictionary<SoapPrimitive, object> QNameToClass = new Dictionary<SoapPrimitive, object>();
        
        /// <summary>
        /// Map from Java class names to XML name and namespace pairs.
        /// </summary>
        protected Dictionary<string, object> ClassToQName = new Dictionary<string, object>();

        /// <summary>
        /// Set to true to add and ID and ROOT label to the envelope. Change to false for compatibility with WSDL.
        /// </summary>
        protected bool AddAdornments = true;
        private readonly Dictionary<object, object> _idMap = new Dictionary<object, object>();
        private List<object> _multiRef;

        public SoapSerializationEnvelope(int version) : base(version)
        {
            AddMapping(Encoding, ArrayMappingName, PropertyInfo.VectorClass);
            DefaultMarshal.Register(this);
        }

        public bool IsAddAdornments()
        {
            return AddAdornments;
        }

        public void SetAddAdornments(bool addAdornments)
        {
            this.AddAdornments = addAdornments;
        }

        /// <summary>
        /// Set the bodyOut to be empty so that no un-needed xml is create. The null value for bodyOut will
        /// cause #writeBody to skip writing anything redundant.
        /// </summary>
        public void SetBodyOutEmpty(bool emptyBody)
        {
            if (emptyBody)
                BodyOut = null;
        }

        public override void ParseBody(XmlPullParser parser)
        {
            BodyIn = null;
            parser.NextTag();
            if (parser.GetEventType() == XmlPullParser.StartTag && parser.GetNamespace().Equals(Envelope)
                && parser.GetName().Equals("Fault"))
            {
                var fault = Version < SoapEnvelope.Ver12 ? new SoapFault(Version) : new SoapFault12(Version);
                fault.Parse(parser);
                BodyIn = fault;
            }
            else
            {
                while (parser.GetEventType() == XmlPullParser.StartTag)
                {
                    var rootAttr = parser.GetAttributeValue(Encoding, RootLabel);
                    var o = Read(parser, null, -1, parser.GetNamespace(), parser.GetName(),
                        PropertyInfo.ObjectType);

                    if ("1".Equals(rootAttr) || BodyIn == null)
                        BodyIn = o;

                    parser.NextTag();
                }
            }
        }

        /// <summary>
        /// Read a SoapObject. This extracts any attributes and then reads the object as a FvmSerializable.
        /// </summary>
        protected void ReadSerializable(XmlPullParser parser, SoapObject obj)
        {
            for (var counter = 0; counter < parser.GetAttributeCount(); counter++)
            {
                var attributeName = parser.GetAttributeName(counter);
                var value = parser.GetAttributeValue(counter);
                obj.AddAttribute(attributeName, value);
            }
            ReadSerializable(parser, (IFvmSerializable) obj);
        }

        /// <summary>
        /// Read a FvmSerializable.
        /// </summary>
        protected void ReadSerializable(XmlPullParser parser, IFvmSerializable obj)
        {
            int tag;
            try
            {
                tag = parser.NextTag();
            }
            catch (XmlPullParserException)
            {
                if (obj is IHasInnerText text)
                    text.SetInnerText(parser.GetText() != null ? parser.GetText() : "");

                tag = parser.NextTag();
            }
            while (tag != XmlPullParser.EndTag)
            {
                var name = parser.GetName();
                if (!ImplicitTypes || !(obj is SoapObject))
                {
                    var info = new PropertyInfo();
                    var propertyCount = obj.GetPropertyCount();
                    var propertyFound = false;

                    for (var i = 0; i < propertyCount && !propertyFound; i++)
                    {
                        info.Clear();
                        obj.GetPropertyInfo(i, Properties, info);

                        if (name.Equals(info.Name) && info.Namespace == null || name.Equals(info.Name) && parser.GetNamespace().Equals(info.Namespace))
                        {
                            propertyFound = true;
                            obj.SetProperty(i, Read(parser, obj, i, null, null, info));
                        }
                    }

                    if (!propertyFound)
                    {
                        if (AvoidExceptionForUnknownProperty)
                        {
                            // Dummy loop to read until corresponding END tag.
                            while (parser.Next() != XmlPullParser.EndTag || !name.Equals(parser.GetName())) { }
                        }
                        else
                        {
                            throw new Exception($"Unknown Property: {name}");
                        }
                    }
                    else
                    {
                        if (obj is IHasAttributes soapObject)
                        {
                            var cnt = parser.GetAttributeCount();
                            var attributeInfo = new AttributeInfo();
                            for (var counter = 0; counter < cnt; counter++)
                            {
                                attributeInfo.SetName(parser.GetAttributeName(counter));
                                attributeInfo.SetValue(parser.GetAttributeValue(counter));
                                attributeInfo.SetNamespace(parser.GetAttributeNamespace(counter));
                                attributeInfo.SetType(parser.GetAttributeType(counter));
                                soapObject.SetAttribute(attributeInfo);

                            }
                        }
                    }
                }
                else
                {
                    ((SoapObject) obj).AddProperty(parser.GetName(), Read(parser, obj, obj.GetPropertyCount(),
                        ((SoapObject) obj).GetNamespace(), name, PropertyInfo.ObjectType));
                }
                try
                {
                    tag = parser.NextTag();
                }
                catch (XmlPullParserException)
                {
                    if (obj is IHasInnerText text)
                        text.SetInnerText(parser.GetText() != null ? parser.GetText() : "");
                    tag = parser.NextTag();
                }

            }
            parser.Require(XmlPullParser.EndTag, null, null);
        }

        /// <summary>
        /// If the type of the object cannot be determined, and thus no Marshal class can handle the object, this
        /// method is called. It will build either a SoapPrimitive or a SoapObject.
        /// </summary>
        protected object ReadUnknown(XmlPullParser parser, string typeNamespace, string typeName)
        {
            var name = parser.GetName();
            var namespace_ = parser.GetNamespace();

            // Cache the Attribute info list from the current element before we move on.
            var attributeInfoVector = new List<object>(); 
            for (var attributeCount = 0; attributeCount < parser.GetAttributeCount(); attributeCount++)
            {
                var attributeInfo = new AttributeInfo();
                attributeInfo.SetName(parser.GetAttributeName(attributeCount));
                attributeInfo.SetValue(parser.GetAttributeValue(attributeCount));
                attributeInfo.SetNamespace(parser.GetAttributeNamespace(attributeCount));
                attributeInfo.SetType(parser.GetAttributeType(attributeCount));
                attributeInfoVector.Add(attributeInfo);
            }

            parser.Next(); // move to Text, inner start tag or end tag.
            object result = null;
            string text = null;
            if (parser.GetEventType() == XmlPullParser.Text)
            {
                text = parser.GetText();
                var sp = new SoapPrimitive(typeNamespace, typeName, text);
                result = sp;

                // Apply all the cached Attribute info list before we add the property and descend further for parsing.
                foreach (var attributeInfo in attributeInfoVector)
                {
                    sp.AddAttribute((AttributeInfo) attributeInfo);
                }
                parser.Next();
            }
            else if (parser.GetEventType() == XmlPullParser.EndTag)
            {
                var so = new SoapObject(typeNamespace, typeName);

                // Apply all the cached Attribute info list before we add the property and descend further for parsing.
                foreach (var attributeInfo in attributeInfoVector)
                {
                    so.AddAttribute((AttributeInfo) attributeInfo);
                }
                result = so;
            }

            if (parser.GetEventType() == XmlPullParser.StartTag)
            {
                if (text != null && text.Trim().Length != 0)
                {
                    throw new Exception("Malformed input: mixed content.");
                }
                var so = new SoapObject(typeNamespace, typeName);
                
                // Apply all the cached Attribute info list before we add the property and descend further for parsing.
                foreach (var attributeInfo in attributeInfoVector)
                {
                    so.AddAttribute((AttributeInfo)attributeInfo);
                }

                while (parser.GetEventType() != XmlPullParser.EndTag)
                {
                    so.AddProperty(parser.GetNamespace(), parser.GetName(), Read(parser, so, so.GetPropertyCount(),
                        null, null, PropertyInfo.ObjectType));
                    parser.NextTag();
                }
                result = so;
            }
            parser.Require(XmlPullParser.EndTag, namespace_, name);
            return result;
        }

        private int GetIndex(string value, int start, int def)
        {
            if (value == null)
            {
                return def;
            }
            try
            {
                return value.Length - start < 3 ? def : int.Parse(value.Substring(start + 1, value.Length - 1 - start));
            }
            catch (Exception)
            {
                return def;
            }
        }

        private static void SetSize(List<object> v, int newSize)
        {
            var dSize = v.Count - newSize;
            if (dSize > 0)
            {
                for (; dSize > 0; dSize--)
                    v.Add(null);
            }
            else if (dSize < 0)
            {
                v.RemoveRange(newSize, -dSize);
            }
        }

        protected void ReadVector(XmlPullParser parser, List<object> v, PropertyInfo elementType)
        {
            string namespace_ = null;
            string name = null;
            var size = v.Count;
            var dynamic = true;
            var type = parser.GetAttributeValue(Encoding, ArrayTypeLabel);
            if (type != null)
            {
                var cut0 = type.IndexOf(':');
                // ReSharper disable once StringIndexOfIsCultureSpecific.2
                var cut1 = type.IndexOf("[", cut0);
                name = type.Substring(cut0 + 1, cut1 - cut0 - 1);
                var prefix = cut0 == -1 ? "" : type.Substring(0, cut0);
                namespace_ = parser.GetNamespace(prefix);
                size = GetIndex(type, cut1, -1);
                if (size != -1)
                {
                    SetSize(v, size);
                    dynamic = false;
                }
            }
            if (elementType == null)
            {
                elementType = PropertyInfo.ObjectType;
            }
            parser.NextTag();
            var position = GetIndex(parser.GetAttributeValue(Encoding, "offset"), 0, 0);
            while (parser.GetEventType() != XmlPullParser.EndTag)
            {
                position = GetIndex(parser.GetAttributeValue(Encoding, "position"), 0, position);
                if (dynamic && position >= size)
                {
                    size = position + 1;
                    SetSize(v, size);
                }

                // Implicit handling of position exceeding specified size.
                v[position] = Read(parser, v, position, namespace_, name, elementType);
                position++;
                parser.NextTag();
            }
            parser.Require(XmlPullParser.EndTag, null, null);
        }

        /// <summary>
        /// This method returns id from the href Attribute value.
        /// </summary>
        protected string GetIdFromHref(string hrefValue)
        {
            return hrefValue.Substring(1);
        }

        /// <summary>
        /// Builds an object from the XML stream. This method is public for usage in conjuction with Marshal
        /// subclasses. Precondition: On the start tag of the object or property, so href can be read.
        /// </summary>
        public object Read(XmlPullParser parser, Object owner, int index, string namespace_, string name,
            PropertyInfo expected)
        {
            var elementName = parser.GetName();
            var href = parser.GetAttributeValue(null, HrefLabel);
            object obj;
            if (href != null)
            {
                if (owner == null)
                {
                    throw new Exception("Href at root level.");
                }
                href = GetIdFromHref(href);
                obj = _idMap[href];
                if (obj == null || obj is FwdRef)
                {
                    var f = new FwdRef {Next = (FwdRef) obj, Obj = owner, Index = index};
                    _idMap[href] = f;
                    obj = null;
                }
                parser.NextTag(); // Start tag.
                parser.Require(XmlPullParser.EndTag, null, elementName);
            }
            else
            {
                var nullAttr = parser.GetAttributeValue(XmlSchemaInstance, NilLabel);
                var id = parser.GetAttributeValue(null, IdLabel);
                if (nullAttr == null)
                {
                    nullAttr = parser.GetAttributeValue(XmlSchemaInstance, NullLabel);
                }
                if (nullAttr != null && StringToBoolean(nullAttr))
                {
                    obj = null;
                    parser.NextTag();
                    parser.Require(XmlPullParser.EndTag, null, elementName);
                }
                else
                {
                    var type = parser.GetAttributeValue(XmlSchemaInstance, TypeLabel);
                    if (type != null)
                    {
                        var cut = type.IndexOf(':');
                        name = type.Substring(cut + 1);
                        var prefix = cut == -1 ? "" : type.Substring(0, cut);
                        namespace_ = parser.GetNamespace(prefix);
                    }
                    else if (name == null && namespace_ == null)
                    {
                        if (parser.GetAttributeValue(Encoding, ArrayTypeLabel) != null)
                        {
                            namespace_ = Encoding;
                            name = ArrayMappingName;
                        }
                        else
                        {
                            object[] names = GetInfo(expected.Type, null);
                            namespace_ = (string) names[0];
                            name = (string) names[1];
                        }
                    }
                    // Make sure to set this flag if we don't know the types.
                    if (type == null)
                        ImplicitTypes = true;
                    obj = ReadInstance(parser, namespace_, name, expected) ?? ReadUnknown(parser, namespace_, name);
                }
                if (id != null)
                    ResolveReference(id, obj);
            }

            parser.Require(XmlPullParser.EndTag, null, elementName);
            return obj;
        }

        protected void ResolveReference(string id, object obj)
        {
            object hlp = _idMap[id];
            if (hlp is FwdRef fwdRef)
            {
                do
                {
                    if (fwdRef.Obj is IFvmSerializable)
                    {
                        ((IFvmSerializable)fwdRef.Obj).SetProperty(fwdRef.Index, obj);
                    }
                    else
                    {
                        ((List<object>)fwdRef.Obj)[fwdRef.Index] = obj;
                    }
                    fwdRef = fwdRef.Next;
                } while (fwdRef != null);
            }
            else if (hlp != null)
                throw new Exception("Double ID.");

            _idMap[id] = obj;
        }

        /// <summary>
        /// Returns a new object read from the given parser. If no mapping is found, null is returned. This method
        /// is used by the SoapParser in order to convert the XML code to Java objects.
        /// </summary>
        public object ReadInstance(XmlPullParser parser, string _namespace, string name, PropertyInfo expected)
        {
            var key = new SoapPrimitive(_namespace, name, null);

            if (!QNameToClass.ContainsKey(key))
            {
                return null;
            }
            var obj = QNameToClass[key];
            if (obj is IMarshal marshal)
                return marshal.ReadInstance(parser, _namespace, name, expected);

            if (obj is SoapObject)
                obj = ((SoapObject) obj).NewInstance();

#pragma warning disable CS0252 // Possible unintended reference comparison; left hand side needs cast
            else if (obj == typeof (SoapObject))
#pragma warning restore CS0252 // Possible unintended reference comparison; left hand side needs cast
            {
                obj = new SoapObject(_namespace, name);
            }
            else
            {
                try
                {
                    obj = Activator.CreateInstance((Type) obj);
                }
                catch (Exception e)
                {
                    throw new Exception(e.ToString());
                }
            }
            if (obj is IHasAttributes soapObject)
            {
                var cnt = parser.GetAttributeCount();
                for (var counter = 0; counter < cnt; counter++)
                {

                    var attributeInfo = new AttributeInfo();
                    attributeInfo.SetName(parser.GetAttributeName(counter));
                    attributeInfo.SetValue(parser.GetAttributeValue(counter));
                    attributeInfo.SetNamespace(parser.GetAttributeNamespace(counter));
                    attributeInfo.SetType(parser.GetAttributeType(counter));
                    soapObject.SetAttribute(attributeInfo);
                }
            }
            
            if (obj is SoapObject soapObj)
                ReadSerializable(parser, soapObj);
            else if (obj is IFvmSerializable text)
            {
                if (text is IHasInnerText innerText)
                {
                    innerText.SetInnerText(parser.GetText() != null ? parser.GetText() : "");
                }
                ReadSerializable(parser, text);

            }
            else if (obj is List<object> list)
                ReadVector(parser, list, expected.ElementType);
            else
                throw new Exception($"No deserializer for {obj.GetType()}.");

            return obj;
        }

        /// <summary>
        /// Returns a string array containing the namespace, name, id and Marshal object for the given java object.
        /// This method is used by the SoapWriter in order to map Java objects to the corresponding SOAP section
        /// five XML code.
        /// </summary>
        public object[] GetInfo(Object type, Object instance)
        {
            if (type == null)
            {
                if (instance is SoapObject || instance is SoapPrimitive)
                    type = instance;
                else
                    type = instance.GetType();
            }
            if (type is SoapObject so)
                return new object[] {so.GetNamespace(), so.GetName(), null, null};

            if (type is SoapPrimitive sp)
                return new object[] {sp.GetNamespace(), sp.GetName(), null, DefaultMarshal};

#pragma warning disable CS0252 // Possible unintended reference comparison; left hand side needs cast
            if (type is Type type1 && type != PropertyInfo.ObjectClass)
#pragma warning restore CS0252 // Possible unintended reference comparison; left hand side needs cast
            {
                var tmp = (object[]) ClassToQName[type1.Name];
                if (tmp != null)
                    return tmp;
            }
            return new object[] {XmlSchemaData, AnyTypeLabel, null, null};
        }

        /// <summary>
        /// Defines a direct mapping from a namespace and name to a java class (and vice versa), using the given
        /// marshal mechanism.
        /// </summary>
        public void AddMapping(string namespace_, string name, Type clazz, IMarshal marshal)
        {
            QNameToClass[new SoapPrimitive(namespace_, name, null)] = marshal ?? (object) clazz;
            ClassToQName[clazz.Name] = new object[] {namespace_, name, null, marshal};
        }

        /// <summary>
        /// Defines a direct mapping from a namespace and name to a java class (and vice versa).
        /// </summary>
        public void AddMapping(string namespace_, string name, Type clazz)
        {
            AddMapping(namespace_, name, clazz, null);
        }

        /// <summary>
        /// Adds a SoapObject to the class map. During parsing, objects of the given type (namespace/name) will be
        /// mapped to corresponding copies of the given SoapObject, maintaining the structure of the template.
        /// </summary>
        public void AddTemplate(SoapObject so)
        {
            QNameToClass[new SoapPrimitive(so.Namespace, so.Name, null)] = so;
        }

        /// <summary>
        /// Response from the soap call. Pulls the object from the wrapper object and returns it.
        /// </summary>
        public object GetResponse()
        {
            if (BodyIn == null)
                return null;

            if (BodyIn is SoapFault fault)
                throw fault;

            if (ParseResponseAsString && BodyIn != null)
                return BodyIn;

            var ks = (IFvmSerializable) BodyIn;

            if (ks.GetPropertyCount() == 0)
                return null;

            if (ks.GetPropertyCount() == 1)
            {
                return ks.GetProperty(0);
            }
            var ret = new List<object>();
            for (var i = 0; i < ks.GetPropertyCount(); i++)
            {
                ret.Add(ks.GetProperty(i));
            }
            return ret;
        }

        /// <summary>
        /// Serializes the request object to the given XmlSerliazer object
        /// </summary>
        public override void WriteBody(IXmlSerializer writer)
        {
            if (BodyOut != null)
            {
                _multiRef = new List<object> {BodyOut};
                var qName = GetInfo(null, BodyOut);

                writer.StartTag((DotNet) ? "" : (string) qName[QNameNamespace], (string) qName[QNameType]);

                if (DotNet)
                    writer.Attribute(null, "xmlns", (string) qName[QNameNamespace]);

                if (AddAdornments)
                {
                    writer.Attribute(null, IdLabel, qName[2] == null ? ("o" + 0) : (string) qName[2]);
                    writer.Attribute(Encoding, RootLabel, "1");
                }
                WriteElement(writer, BodyOut, null, qName[QNameMarshal]);
                writer.EndTag((DotNet) ? "" : (string) qName[QNameNamespace], (string) qName[QNameType]);
            }
        }

        private void WriteAttributes(IXmlSerializer writer, IHasAttributes soapObject)
        {
            var cnt = soapObject.GetAttributeCount();
            for (var counter = 0; counter < cnt; counter++)
            {
                var attributeInfo = new AttributeInfo();
                soapObject.GetAttributeInfo(counter, attributeInfo);
                soapObject.GetAttribute(counter, attributeInfo);
                if (attributeInfo.GetValue() != null)
                {
                    writer.Attribute(attributeInfo.GetNamespace(), attributeInfo.GetName(),
                        attributeInfo.GetValue().ToString());
                }
            }
        }

        public void WriteArrayListBodyWithAttributes(IXmlSerializer writer, IFvmSerializable obj)
        {
            if (obj is IHasAttributes attributes)
                WriteAttributes(writer, attributes);

            // ReSharper disable once SuspiciousTypeConversion.Global
            WriteArrayListBody(writer, (List<object>) obj);
        }

        public void WriteObjectBodyWithAttributes(IXmlSerializer writer, IFvmSerializable obj)
        {
            if (obj is IHasAttributes attributes)
                WriteAttributes(writer, attributes);
            WriteObjectBody(writer, obj);
        }

        /// <summary>
        /// Writes the body of an FvmSerializable object. This method is public for access from Marshal subclasses.
        /// </summary>
        public void WriteObjectBody(IXmlSerializer writer, IFvmSerializable obj)
        {
            var cnt = obj.GetPropertyCount();
            var propertyInfo = new PropertyInfo();
            for (var i = 0; i < cnt; i++)
            {
                var prop = obj.GetProperty(i);
                obj.GetPropertyInfo(i, Properties, propertyInfo);

                if (!(prop is SoapObject))
                {
                    if ((propertyInfo.Flags & PropertyInfo.Transient) == 0)
                    {
                        var objValue = obj.GetProperty(i);
                        if ((prop != null || !SkipNullProperties) && (objValue != SoapPrimitive.NullSkip))
                        {
                            writer.StartTag(propertyInfo.Namespace, propertyInfo.Name);
                            WriteProperty(writer, objValue, propertyInfo);
                            writer.EndTag(propertyInfo.Namespace, propertyInfo.Name);
                        }
                    }
                }
                else
                {
                    var nestedSoap = (SoapObject) prop;
                    var qName = GetInfo(null, nestedSoap);
                    string _namespace;
                    var type = (string) qName[QNameType];

                    string name;
                    if (!string.IsNullOrEmpty(propertyInfo.Name))
                        name = propertyInfo.Name;
                    else
                        name = (string) qName[QNameType];

                    if (!string.IsNullOrEmpty(propertyInfo.Namespace))
                        _namespace = propertyInfo.Namespace;
                    else
                        _namespace = (string) qName[QNameNamespace];

                    writer.StartTag(_namespace, name);
                    if (!ImplicitTypes)
                    {
                        var prefix = writer.GetPrefix(_namespace, true);
                        writer.Attribute(XmlSchemaInstance, TypeLabel, prefix + ":" + type);
                    }
                    WriteObjectBodyWithAttributes(writer, nestedSoap);
                    writer.EndTag(_namespace, name);
                }
            }
            if (obj is IHasInnerText text)
            {
                if (text.GetInnerText() != null)
                    writer.CdSect(text.GetInnerText());
            }
        }

        protected void WriteProperty(IXmlSerializer writer, object obj, PropertyInfo type)
        {
            if (obj == null || obj == SoapPrimitive.NullNilElement)
            {
                writer.Attribute(XmlSchemaInstance, Version >= Ver12 ? NilLabel : NullLabel, "true");
                return;
            }
            var qName = GetInfo(null, obj);
            if (type.MultiReference || qName[2] != null)
            {
                var i = _multiRef.IndexOf(obj);
                if (i == -1)
                {
                    i = _multiRef.Count;
                    _multiRef.Add(obj);
                }
                writer.Attribute(null, HrefLabel, qName[2] == null ? ("#o" + i) : "#" + qName[2]);
            }
            else
            {
#pragma warning disable CS0253 // Possible unintended reference comparison; right hand side needs cast
                if (!ImplicitTypes || obj.GetType() != type.Type)
#pragma warning restore CS0253 // Possible unintended reference comparison; right hand side needs cast
                {
                    var prefix = writer.GetPrefix((string) qName[QNameNamespace], true);
                    writer.Attribute(XmlSchemaInstance, TypeLabel, prefix + ":" + qName[QNameType]);
                }
                WriteElement(writer, obj, type, qName[QNameMarshal]);
            }
        }

        protected void WriteElement(IXmlSerializer writer, object element, PropertyInfo type, Object marshal)
        {
            if (marshal != null)
                ((IMarshal) marshal).WriteInstance(writer, element);
            else if (element is IFvmSerializable || element == SoapPrimitive.NullNilElement || element == SoapPrimitive.NullSkip)
            {
                if (element is List<object>)
                    WriteArrayListBodyWithAttributes(writer, (IFvmSerializable) element);
                else
                    WriteObjectBodyWithAttributes(writer, (IFvmSerializable) element);
            }
            else if (element is IHasAttributes attributes)
                WriteAttributes(writer, attributes);
            else if (element is List<object> list)
                WriteVectorBody(writer, list, type.ElementType);
            else
                throw new Exception($"Cannot serialize: {element}.");
        }

        protected void WriteArrayListBody(IXmlSerializer writer, List<object> list)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            var obj = (IFvmSerializable) list;
            var cnt = list.Count;
            var propertyInfo = new PropertyInfo();
            for (var i = 0; i < cnt; i++)
            {
                var prop = obj.GetProperty(i);
                obj.GetPropertyInfo(i, Properties, propertyInfo);

                if (!(prop is SoapObject))
                {
                    if ((propertyInfo.Flags & PropertyInfo.Transient) != 0)
                        continue;

                    var objValue = obj.GetProperty(i);
                    if (prop == null && SkipNullProperties || objValue == SoapPrimitive.NullSkip)
                        continue;

                    writer.StartTag(propertyInfo.Namespace, propertyInfo.Name);
                    WriteProperty(writer, objValue, propertyInfo);
                    writer.EndTag(propertyInfo.Namespace, propertyInfo.Name);
                }
                else
                {
                    var nestedSoap = (SoapObject) prop;
                    var qName = GetInfo(null, nestedSoap);
                    string _namespace;
                    var type = (string) qName[QNameType];

                    string name;
                    if (!string.IsNullOrEmpty(propertyInfo.Name))
                        name = propertyInfo.Name;
                    else
                        name = (string) qName[QNameType];

                    if (!string.IsNullOrEmpty(propertyInfo.Namespace))
                        _namespace = propertyInfo.Namespace;
                    else
                        _namespace = (string) qName[QNameNamespace];

                    writer.StartTag(_namespace, name);
                    if (!ImplicitTypes)
                    {
                        var prefix = writer.GetPrefix(_namespace, true);
                        writer.Attribute(XmlSchemaInstance, TypeLabel, prefix + ":" + type);
                    }
                    WriteObjectBodyWithAttributes(writer, nestedSoap);
                    writer.EndTag(_namespace, name);
                }
            }

            if ((obj as IHasInnerText)?.GetInnerText() != null)
                writer.CdSect(((IHasInnerText) obj).GetInnerText());
        }

        protected void WriteVectorBody(IXmlSerializer writer, List<object> vector, PropertyInfo elementType)
        {
            var itemsTagName = ItemLabel;
            string itemsNamespace = null;

            if (elementType != null)
            {
                var info = elementType;
                if (info.Name != null)
                {
                    itemsTagName = info.Name;
                    itemsNamespace = info.Namespace;
                }
            }
            else
                elementType = PropertyInfo.ObjectType;

            var cnt = vector.Count;
            var arrType = GetInfo(elementType.Type, null);

            if (!ImplicitTypes)
                writer.Attribute(Encoding, ArrayTypeLabel, writer.GetPrefix((string) arrType[0], false) + ":" + arrType[1] + "[" + cnt + "]");
            else
            {
                if (itemsNamespace == null)
                    itemsNamespace = (string) arrType[0];
            }

            var skipped = false;
            for (var i = 0; i < cnt; i++)
            {
                if (vector[i] == null)
                    skipped = true;
                else
                {
                    writer.StartTag(itemsNamespace, itemsTagName);
                    if (skipped)
                    {
                        writer.Attribute(Encoding, "position", "[" + i + "]");
                        skipped = false;
                    }
                    WriteProperty(writer, vector[i], elementType);
                    writer.EndTag(itemsNamespace, itemsTagName);
                }
            }
        }
    }
}
