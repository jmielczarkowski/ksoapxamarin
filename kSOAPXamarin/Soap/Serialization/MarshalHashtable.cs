﻿using System;
using System.Collections.Generic;
using kSOAPXamarin.Xml.XmlPull;

namespace kSOAPXamarin.Soap.Serialization
{
    /// <summary>
    /// Serializes instances of hashtable to and from xml. This implementation is
    /// based on the xml schema from apache-soap, namely the type 'map' in the
    /// namespace 'http://xml.apache.org/xml-soap'. Other soap implementations
    /// including apache (obviously) and glue are also interoperable with the schema.
    /// </summary>
    public class MarshalHashtable : IMarshal
    {
        /// <summary>
        /// Use then during registration.
        /// </summary>
        public static readonly string Namespace = "http://xml.apache.org/xml-soap";

        /// <summary>
        /// Use then during registration.
        /// </summary>
        public static readonly string Name = "Map";

        public static readonly Type HashtableClass = new Dictionary<object, object>().GetType();

        private SoapSerializationEnvelope _envelope;

        public object ReadInstance(XmlPullParser parser, string namespace_, string name, PropertyInfo expected)
        {
            var instance = new Dictionary<object, object>();
            var elementName = parser.GetName();
            while (parser.NextTag() != XmlPullParser.EndTag)
            {
                var item = new ItemSoapObject(instance);
                parser.Require(XmlPullParser.StartTag, null, "item");
                parser.NextTag();
                var key = _envelope.Read(parser, item, 0, null, null, PropertyInfo.ObjectType);
                parser.NextTag();
                if (key != null)
                    item.SetProperty(0, key);

                var value = _envelope.Read(parser, item, 1, null, null, PropertyInfo.ObjectType);
                parser.NextTag();
                if (value != null)
                    item.SetProperty(1, value);

                parser.Require(XmlPullParser.EndTag, null, "item");
            }

            parser.Require(XmlPullParser.EndTag, null, elementName);
            return instance;
        }

        public void WriteInstance(IXmlSerializer writer, object instance)
        {
            var soapDictionary = instance as Dictionary<object, object>;
            var item = new SoapObject(null, null);
            item.AddProperty("key", null);
            item.AddProperty("value", null);
            if (soapDictionary == null)
                return;

            foreach (var keyValuePair in soapDictionary)
            {
                writer.StartTag("", "item");
                item.SetProperty(0, keyValuePair.Key);
                item.SetProperty(1, keyValuePair.Value);
                _envelope.WriteObjectBodyWithAttributes(writer, item);
                writer.EndTag("", "item");
            }
        }

        private class ItemSoapObject : SoapObject
        {
            private readonly Dictionary<object, object> _soapDictionary;
            private int _resolvedIndex = -1;

            public ItemSoapObject(Dictionary<object, object> soapDictionary) : base(null, null)
            {
                _soapDictionary = soapDictionary;
                AddProperty("key", null);
                AddProperty("value", null);
            }

            /// <summary>
            /// Sets property, only 1 or 0.
            /// </summary>
            public override void SetProperty(int index, object value)
            {
                if (_resolvedIndex == -1)
                {
                    base.SetProperty(index, value);
                    _resolvedIndex = index;
                }
                else
                {
                    var resolved = _resolvedIndex == 0 ? GetProperty(0) : GetProperty(1);
                    if (index == 0)
                        _soapDictionary[value] = resolved;
                    else
                        _soapDictionary[resolved] = value;
                }
            }
        }

        public void Register(SoapSerializationEnvelope cm)
        {
            _envelope = cm;
            cm.AddMapping(Namespace, Name, HashtableClass, this);
        }
    }
}
