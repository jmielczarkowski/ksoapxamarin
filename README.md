## About

This is .NET fork of kSOAP library used in Android (natively supported as jar). 
In Xamarin the best way would be to use Binding Library and come with API definition for latest kSOAP jar.

However if there are any good reasons that prevent you from following that approach - be my guest and reuse this library.
I've added possibility to disable some of XmlPullParser features and allow you to decide how to deal with responses.

Keep in mind that this is old version of library and most of code was written few years ago. 
If you need latest version which contains: unit / integration tests, bug fixes and improvements please contact me directly.

## Usage

###1. Define URL, METHOD_NAME, SOAP_ACTION and NAMESPACE for your SOAP request. Example:
---

	public string URL = "http://YOUR_URL:YOUR_PORT/Basic?wsdl";
	
	public string METHOD_NAME = "YOUR_METHOD_NAME";
	
	public string SOAP_ACTION = "http://tempuri.org/YOUR_SERVICE_NAME/METHOD_NAME";
	
	public string NAMESPACE = "http://tempuri.org/";
	
	public int SOAP_VERSION = SoapEnvelope.Ver11;
	
	
	
###2. Try to use it with following setup:
---

	SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SOAP_VERSION);
	
	envelope.SetOutputSoapObject(request);
	
	envelope.DotNet = true;

	HttpTransportSE httpTransport = new HttpTransportSE(URL);
	
	httpTransport.Call(SOAP_ACTION, envelope);

	var response = envelope.GetResponse();
	
###3. If for any reason your SOAP response can't be parsed with XmlPullParser please consider adding following setup:
---
	
	envelope.ParseResponseAsString = true;